#!/bin/bash

while read -r f; do
    echo "> $f"
    if pcregrep -M --color=always -a -n '[ \r]+$' "$f"; then
        dos2unix -k "$f"
        sed -E -i 's/ +$//g' "$f"
    fi
done < <(find . -type f \( -iname '*.java' \) -printf '%P\n')
