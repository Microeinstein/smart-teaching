\myparskip%
Per il processo di sviluppo è stato scelto di adottare la tecnica \textit{agile}. Nonostante ciò si è partiti dando ampio spazio all'analisi dei requisiti e alla progettazione, realizzando quindi una versione iniziale di tutti i diagrammi UML.\ Solo dopo aver definito chiaramente quali fossero le componenti essenziali per la creazione del software, è cominciata l'implementazione; durante questa fase si è cercato di contenere al minimo le eventuali aggiunte al progetto iniziale.

Al completamento dell'implementazione di una caratteristica del progetto è seguita un'esaustiva attività di test manuale, ed è stato eseguito un refactoring laddove ritenuto necessario.

\subsection{Progettazione e pattern architetturali}

La progettazione del sistema è stata svolta utilizzando il paradigma della programmazione ad oggetti.
La scelta riguardante l'architettura da utilizzare è ricaduta sul pattern MVC, ritenuta l'opzione più valida ed ovvia. Infatti la suddivisione logica delle operazioni nelle parti Model, View e Controller (specificata in seguito) offre una miglior efficacia in caso di modifiche o estensioni: è possibile intervenire su un determinato blocco di codice, lasciando intatti gli altri. I compiti assegnati a ciascuna suddivisione MVC sono i seguenti:
\begin{custlist}[,leftmargin=5.5em]
  \item[\textbf{Model}] si occupa della gestione dei dati e della loro estrazione dal database;
  \item[\textbf{View}] specifica la forma di presentazione dei dati dall'utente, ovvero l'interfaccia grafica;
  \item[\textbf{Controller}] implementa la logica di controllo, ovvero come il sistema reagisce agli input (attraverso i listener) e modifica il model o la view se necessario.
\end{custlist}

Per una maggior chiarezza architetturale le classi appartenenti ai campi MVC sono state suddivise nei relativi packages \textendash{} tutte le classi che si occupano dell'interazione con l'utente sono state inserite nel package \code{View} e le classi che si occupano di creare gli oggetti e (de)serializzare si trovano nel package \code{Model}.
Inoltre per quanto riguarda le classi contenute nel package \code{View} è stata principalmente usata la libreria Swing, data la sua compatibilità con l'architettura MVC.
\clearpage

\paragraph{Gestione dei dati}

Durante l'analisi dei requisiti di sistema, è emersa sin da subito un'elevata quantità di riferimenti e associazioni \textendash{} con varie molteplicità \textendash{} tra gruppi di informazioni a sé stanti. Essendo che il sistema necessita di salvare e caricare dati e relazioni da una memoria di massa, è stato individuato un problema non banale: gestire relazioni ricorsive o reciproche (oggetto \code{X} ha un riferimento ad un oggetto \code{Y} e viceversa).

La soluzione che è stata adottata, consiste nell'associare ad ogni oggetto serializzabile un identificativo univoco; al salvataggio di un determinato oggetto vengono scritti tutti i suoi campi con valori primitivi (\code{int}, \code{String}, \code{enum}\dots), mentre al posto di navigare ricorsivamente i riferimenti ad altri oggetti, di questi viene salvato solamente il codice identificativo.

Inoltre per evitare il caricamento di riferimenti ricorsivi, durante la progettazione si è cercato di modellare le associazioni tra classi con una struttura ad albero. Sempre per lo stesso motivo, alcuni campi presenti nel \textit{database} vengono appositamente ignorati durante la deserializzazione dei singoli oggetti, ma comunque utilizzati in altri contesti.

Nei seguenti diagrammi sono state adottate alcune inconformità: 
\begin{custlist}[]
  \item[\P] indica una classe specifica del prototipo
  \item[\code{@}] indica una classe o un metodo astratti  
  \item[\code{.}] indica un campo presente nei dati del prototipo ma non nella classe 
\end{custlist} 
\clearpage
\largepage%
\image[1]{../final/classes2.png}{Diagramma delle classi del modello (1/2)}{classes_a}
\clearpage
\image[1]{../final/classes.png}{Diagramma delle classi del modello (2/2)}{classes_b}
\begin{center}
  Nota: tutte le classi vengono create da, e fanno riferimento a \code{Model}
\end{center} 
\clearpage
\image[1]{../final/classes3.png}{Diagramma delle classi della vista-controllore}{classes_c}
\clearpage
\image[1]{../final/classes4.png}{Diagramma delle classi di supporto}{classes_d}
\clearpage
\restoregeometry%

\subsection{Implementazione e design pattern}

Durante lo sviluppo del progetto si è fatto riferimento a diversi design pattern.

\begin{custlist}
  \item Per le classi \code{Model} e \code{Controller} è stato adottato il \textbf{Pattern Singleton}: durante tutta l'esecuzione del software viene istanziato ed usato un unico oggetto statico per ciascuna classe.
  \item Data la natura ad oggetti del linguaggio Java, è stato implicitamente adottato il \textbf{Pattern Iterator}.
  \item Riguardo l'accesso ai dati si è deciso di ricorrere al \textbf{Data Access Object Pattern}. Infatti il sistema, per ottenere i dati dei Docenti, Studenti e Lezioni si interfaccia alla classe \code{Model} (implementata in \code{JSONModel}), che gestisce direttamente il database. Questo implica anche un buon livello di sicurezza per i dati poiché l'utente non interagisce direttamente con essi.
  \item È stato adottato anche il \textbf{Pattern Decorator} in caso fosse necessario aggiungere piccole funzionalità o cambiamenti di stile: ne è un esempio il metodo \code{setText} nella classe \code{JLink}, che modifica adeguatamente il font-size di un particolare testo.
\end{custlist}

Per garantire una miglior presentazione visiva si è deciso di ricorrere al meccanismo dei pannelli, in modo da evitare di creare nuove finestre ogni qualvolta si passi ad una nuova schermata. I cambiamenti di schermata e le varie azioni compiute dall'utente sono ricevute dagli ActionListener.

Seguono alcuni ulteriori diagrammi di sequenza indicativi di specifiche logiche interne del sistema, attraverso un moderato livello di dettaglio.
\clearpage
\largepage%
\image[1]{../final/sequence_login.png}{Sequenza di avvio e login}{seqmainlogin}
\clearpage
\image[.8]{../final/sequence_download.png}{Sequenza di apertura vista materiali e download}{seqmatdown}
\clearpage
\restoregeometry%
