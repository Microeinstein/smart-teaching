* Produrre:
    * prototipo
        * GUI
    * documentazione progetto
        * use case, schede di specifica
            * sequence diagram dettagliata
        * activity diagram: modalità interazione/operatività software
        * class+sequence diagrams software progettato
        * (breve) descrizione attività test prototipo
        * discussione pattern adottati, applicazione nel prototipo


* supporto di didattica universitaria
* anche modalità smart working

* differenti anni accademici
    * differente erogazione degli insegnamenti
    * differenti docenti
    * differenti contenuti

* informazioni inserite dagli amministratori del sistema
    * insegnamenti
    * corsi di laurea 

* 1 anno accademico:
    * 1+ insegnamenti:
        * 1+ docenti
        * 1+ studenti

* 1 laurea
	* tipo
	* responsabile
		* magistrali/triennali
			* +insegnamenti
			* titolo

* 1 insegnamento:
    * + `Lauree = LaureeTriennali/LaureeMagistrali` oppure insegnamenti diversi fra loro con stesso nome
    * titolare + altri docenti:
        * `listaDocenti + indiceTitolare + (metodi)`
    * programma (stringa)
    * + lezioni 
    * ore complessive; `tempoRimasto()`
	* lista studenti iscritti + data ultimo accesso alla piattaforma
    
* 1 lezione:
    * riferimento insegnamento
    * `indiceDocente + (metodi)`
    * durata
    * in presenza / streaming / solo materiale (-> +riferimento al video)
    * (0+) materiale didattico (video | lucidi | esercizi | ...)
    * (0+) compiti assegnati

* 1 compito:
    * riferimento lezione
	* descrizione
	* valutazioni 
	* data di scadenza 
	* data di assegnazione
	* consegna da parte di uno studente

* 1 studente `extends Persona`

* 1 docente `extends Persona`

* 1 persona
	* ID
	* nome e cognome (1 attributo solo)
	* mail
	* insegnamenti
    
