* (any):
    * ~~ultimo accesso~~
    * ~~chat~~

* lauree:
    * ~~_associazione json laurea-insegnamento_~~
    * ~~associazione json studente-laurea~~
        * ~~_mostra solo altri insegnamenti del corso di laurea_~~
    * HOME
        * se docente:
            * ~~mostra insegnamenti che tiene~~
            * MENU/altri insegnamenti:
                * se responsabile di qualche corso laurea:
                    * metti all'inizio gli insegnamenti, ~~permetti visione~~
                    ~~(solo iscritti + materiale)~~
                * ~~altrimenti nascondi altri insegnamenti~~

* chat:
    * ~~dati json~~
    * ~~modello~~
    * ~~GUI~~
    * ~~links~~
    * ~~nuova chat:~~
        * ~~diretto (singolo)~~
        * ~~broadcast (multiplo)~~
    * ~~lista partecipanti broadcast~~

* diagrammi:
    * ~~aggiornare diagramma classi finale~~
    * ~~sequence diagram finale~~

* relazione:
    * processo di sviluppo: 
      * ~~tecnica agile~~
      * ~~prima progettazione poi implementazione~~
      * ~~diagrammi UML ad inizio sviluppo, poi modificati successivamente~~
      * ~~test ad ogni iterazione~~
