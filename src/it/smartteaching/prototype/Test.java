package it.smartteaching.prototype;

import it.smartteaching.prototype.model.*;
import static it.smartteaching.prototype.model.Model.MODEL;
import static it.smartteaching.prototype.model.classiLezione.*;
import static it.smartteaching.prototype.model.classiPersone.*;
//import static it.smartteaching.prototype.model.classiConferencePlatform.*;
import java.time.LocalDateTime;
import java.util.*;
//import java.util.stream.Collectors;
//import java.lang.*;
//import org.json.*;

public class Test {
    public static void test0() {
        /*HashSet<Laurea> l = new HashSet<>(Arrays.asList(
            new Laurea(TipoLaurea.MAGISTRALE, "Prog1", new Docente(null, null, null)),
            new Laurea(TipoLaurea.MAGISTRALE, "Prog2", new Docente(null, null, null))
        ));
        new Lauree(TipoLaurea.TRIENNALE, l);*/
    }
    public static void test1() throws Core.InvalidCredentialsException {
        Persona p = MODEL.login("matricola", "password");
        System.err.println(p);

        List<Insegnamento> ins = MODEL.getInsegnamenti(p, true);
        for (Insegnamento i : ins) {
            System.err.println(i);
        }
    }
    public static void test2() {
        Persona p;
        //p = MODEL.getIdentity("matricola");
        p = MODEL.getIdentity("professore");
        Insegnamento i = MODEL.getInsegnamenti(p, true).get(0);
        Lezione l;
        //l = i.getLezioni().get(0);
        l = new LezionePresenza("test", "hmm", i, (Docente)p, 30, LocalDateTime.now());
        MODEL.modificaLezione(l, Model.DataOperation.ADD, null, null);
        MODEL.modificaLezione(l, Model.DataOperation.UPDATE, null, null);
        MODEL.modificaLezione(l, Model.DataOperation.DELETE, null, null);
    }

    public static void main(String[] args) throws Exception {
        //test0();
        //test1();
        test2();
    }
}
