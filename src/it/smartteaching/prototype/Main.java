package it.smartteaching.prototype;

import it.smartteaching.prototype.control.Controller;

public class Main {
    public static void main(String args[]) {
        Controller.CONTROLLER.run();
    }
}
