package it.smartteaching.prototype;

import static java.lang.Math.*;
import edu.stanford.ejalbert.*;
import edu.stanford.ejalbert.exception.BrowserLaunchingInitializingException;
import edu.stanford.ejalbert.exception.UnsupportedOperatingSystemException;
import it.smartteaching.prototype.model.Model.DataOperation;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Font;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.*;
import java.util.logging.*;
import javax.swing.*;
import java.net.URI;
import java.nio.file.Path;
import java.util.stream.Collectors;

public class Core {
    public static interface Action {
        void call();
    }
    public static interface IReadOnly {
        boolean isReadOnly();
    }
    public static interface IDataUserEditor<T> {
        boolean isApproved();
        DataOperation getChoice();
        T applyData();
        default Changes<?> getChanges(String name) {
            throw new IllegalArgumentException("Unknown name.");
        }
        void undoPending();
    }
    public static class Pair<A,B> {
        public A fst;
        public B snd;
        public Pair(A a, B b) {
            this.fst = a;
            this.snd = b;
        }
    }
    public static class Changes<T> {
        public final Map<DataOperation, List<T>> changes;
        public Changes() {
            changes = new HashMap<>();
            for (DataOperation op : DataOperation.values())
                changes.put(op, new ArrayList<>());
        }
        public void perform(T obj, DataOperation op) {
            List<T> lcA, lcU, lcD;
            switch (op) {
                case ADD:
                    lcA = changes.get(DataOperation.ADD);
                    if (lcA.contains(obj)) return;
                    lcU = changes.get(DataOperation.UPDATE);
                    if (lcU.contains(obj)) return;
                    lcD = changes.get(DataOperation.DELETE);
                    if (lcD.contains(obj)) {
                        lcD.remove(obj);
                        lcU.add(obj);
                        return;
                    }
                    lcA.add(obj);
                    break;

                case UPDATE:
                    lcA = changes.get(DataOperation.ADD);
                    if (lcA.contains(obj)) return;
                    lcU = changes.get(DataOperation.UPDATE);
                    if (lcU.contains(obj)) return;
                    lcD = changes.get(DataOperation.DELETE);
                    if (lcD.contains(obj)) throw new RuntimeException("Trying to update deleted object.");
                    lcU.add(obj);
                    break;

                case DELETE:
                    lcD = changes.get(DataOperation.DELETE);
                    if (lcD.contains(obj)) return;
                    lcA = changes.get(DataOperation.ADD);
                    if (lcA.contains(obj)) {
                        lcA.remove(obj);
                        lcD.add(obj);
                        return;
                    }
                    lcU = changes.get(DataOperation.UPDATE);
                    if (lcU.contains(obj)) {
                        lcU.remove(obj);
                        lcD.add(obj);
                        return;
                    }
                    lcD.add(obj);
                    break;
            }
        }
    }
    public static class InvalidCredentialsException extends Exception { }
    public static class MissingRequiredFieldException extends RuntimeException { }
    public static class DataIsNotCoherentException extends RuntimeException { }

    public static final DateTimeFormatter CustomDateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    public static final BrowserLauncher browser;
    public static final Base64.Encoder b64e = Base64.getEncoder();

    static {
        try {
            browser = new BrowserLauncher();
        } catch (BrowserLaunchingInitializingException
               | UnsupportedOperatingSystemException ex) {
            throw new ExceptionInInitializerError();
        }
    }

    public static void assertNonEmpty(Collection c, String argname) {
        if (c.isEmpty())
            throw new IllegalArgumentException(
                "La collezione deve contenere almeno 1 oggetto.",
                new Throwable(argname)
            );
    }
    public static void assertFileExists(Path p) {
        if (!p.toFile().isFile())
            throw new RuntimeException("File mancante.");
    }
    public static String autoString(Object o) {
        StringBuilder sb = new StringBuilder();
        Class c = o.getClass();
        sb.append(String.format("[%s] {\n", c.getName()));
        List.of(c.getFields()).stream()
            .map(f -> {
                try {
                    Object v = f.get(o);
                    return String.format("%s = %s\n",
                        f.getName(),
                        (v == null)? "<null>" : v.toString()
                    );
                } catch (IllegalArgumentException |
                           IllegalAccessException |
                             NullPointerException ex) {
                    return "";
                }
            })
            .forEach(s -> List.of(s.split("\n")).forEach(l -> {
                sb.append("  ");
                sb.append(l);
                sb.append("\n");
            }));
        sb.append("}");
        return sb.toString();
    }
    public static void timeSet(int totalMin, BiConsumer<Integer, Integer> cons) {
        int hour = totalMin / 60;
        totalMin -= hour * 60;
        cons.accept(hour, totalMin);
    }
    public static <A,B> Pair<A,B> tuple(Object... objs) {
        if (objs.length <= 2)
            return new Pair<>((A)objs[0], (B)objs[1]);
        return new Pair<>((A)objs[0], (B)tuple(Arrays.copyOfRange(objs, 1, objs.length)));
    }
    public static String genid() {
        return UUID.randomUUID().toString();
    }
    public static String join(String sep, String... parts) {
        return Arrays.stream(parts).filter(p -> p != null).collect(Collectors.joining(sep));
    }
    public static char randchar(Random rnd, char a, char b) {
        return (char)(a + (char)rnd.nextInt(b - a + 1));
    }
    public static <T> T randin(Random rnd, T... objs) {
        return objs[rnd.nextInt(objs.length)];
    }
    public static <T> T randfrom(Random rnd, int len, Function<Integer, T> getter) {
        return getter.apply(rnd.nextInt(len));
    }
    public static void openLink(String link) {
//        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
//        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
//            try {
//                desktop.browse(URI.create(link));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
        browser.openURLinBrowser(link);
    }

    public static final String[] BYTES_SCALES = {"", "K", "M", "G", "T"};
    public static String niceBytes(long totalbytes) {
        int degree = min((int)(totalbytes != 0 ? log(abs(totalbytes)) / log(1024.0) : 0), 3);
        double scaled = totalbytes / (pow(1024, degree));
        if (degree == 0)
            return String.format("%dB", (int)scaled);
        return String.format("%.2f%siB", scaled, BYTES_SCALES[degree]);
    }

    public static void setLookAndFeel() {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        UIManager.LookAndFeelInfo[] lookAndFeels = UIManager.getInstalledLookAndFeels();
        for (UIManager.LookAndFeelInfo info : lookAndFeels)
            System.err.println(info.getName());

        try {
            for (UIManager.LookAndFeelInfo info : lookAndFeels) {
                if ("GTK+".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (
            ClassNotFoundException |
            InstantiationException |
            IllegalAccessException |
            UnsupportedLookAndFeelException ex
        ) {
            Logger.getLogger("SmartTeaching").log(Level.SEVERE, null, ex);
        }
    }
    public static void setSameFontSize(JLabel l, int size) {
        Font f = l.getFont();
        l.setFont(f.deriveFont(f.getStyle(), size));
    }
    public static String toMultilineHTML(String txt) {
        return "<html>" + txt.replace("\n", "<br>") + "</html>";
    }
    public static void removeComp(Component c) {
        Container parent = c.getParent();
        c.setVisible(false);
        c.setEnabled(false);
        parent.getLayout().removeLayoutComponent(c);
        parent.remove(c);
        parent.revalidate();
    }
}
