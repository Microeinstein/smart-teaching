package it.smartteaching.prototype.model;

import static it.smartteaching.prototype.Core.*;
import it.smartteaching.prototype.model.Model.Identifiable;
import it.smartteaching.prototype.model.classiLaurea.Laurea;
import java.time.*;
import java.util.Objects;

public interface classiPersone {
    public static enum TipoPersona {
        STUDENTE,
        DOCENTE
    }

    public static abstract class Persona extends Identifiable<String> implements Comparable<Persona> {
        public final TipoPersona tipo;
        public final String nome;
        public LocalDateTime ultimaVisita;

        public Persona(String id, TipoPersona tipo, String nome, LocalDateTime ultimaVisita) {
            super(id);
            this.tipo = tipo;
            this.nome = nome;
            this.ultimaVisita = ultimaVisita;
        }
        public String getLastVisitMsg() {
            return String.format("ultimo accesso: %s", CustomDateTimeFormat.format(ultimaVisita));
        }

        @Override
        public String toString() {
            return this.nome;
        }
        @Override
        public boolean equals(Object b) {
            if (!(b instanceof Persona))
                return false;
            return this.id.equals(((Persona)b).id);
        }
        @Override
        public int hashCode() {
            int hash = 7;
            hash = 13 * hash + Objects.hashCode(this.id);
            return hash;
        }
        @Override
        public int compareTo(Persona t) {
            return this.nome.compareTo(t.nome);
        }
    }

    public static class Studente extends Persona {
        public Studente(String id, String nome, LocalDateTime ultimaVisita) {
            super(id, TipoPersona.STUDENTE, nome, ultimaVisita);
        }
    }

    public static class Docente extends Persona {
        public Docente(String id, String nome, LocalDateTime ultimaVisita) {
            super(id, TipoPersona.DOCENTE, nome, ultimaVisita);
        }
    }
}
