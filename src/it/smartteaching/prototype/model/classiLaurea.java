package it.smartteaching.prototype.model;

import static it.smartteaching.prototype.Core.*;
import it.smartteaching.prototype.model.Model.Identifiable;
import static it.smartteaching.prototype.model.Model.MODEL;
import static it.smartteaching.prototype.model.classiPersone.*;
import java.util.*;
import java.util.stream.Collectors;

public interface classiLaurea {
    public static enum TipoLaurea {
        TRIENNALE,
        MAGISTRALE
    }

    public static class Laurea extends Identifiable<String> {
        public final TipoLaurea tipo;
        public final String     nome;
        public final Docente    responsabile;

        public Laurea(String id, TipoLaurea tipo, String nome, Docente responsabile) {
            super(id);
            this.tipo         = tipo;
            this.nome         = nome;
            this.responsabile = responsabile;
        }

        public List<Insegnamento> getInsegnamenti() {
            return MODEL.getInsegnamenti(this);
        }
    }
}
