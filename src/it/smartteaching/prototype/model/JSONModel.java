package it.smartteaching.prototype.model;

import static it.smartteaching.prototype.model.JSONReflect.*;
import static it.smartteaching.prototype.model.classiPersone.*;
import static it.smartteaching.prototype.model.classiLezione.*;
import static it.smartteaching.prototype.model.classiFile.*;
import static it.smartteaching.prototype.model.classiChat.*;
import static it.smartteaching.prototype.Core.*;
import it.smartteaching.prototype.Core.Pair;
import it.smartteaching.prototype.model.classiLaurea.Laurea;
import java.util.*;
import java.io.*;
import java.nio.file.*;
import java.time.LocalDateTime;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.json.*;

public class JSONModel implements Model {
    public final Path folder;
    public final Path
        path_credentials,
        path_identities,
        path_insegnamenti,
        path_lauree,
        path_chat_recenti,
        path_chat;
    public final JSONObject
        credentials,
        identities,
        insegnamenti,
        lauree,
        chat_recenti,
        chat;

    public static <T> T readFile(Path path) {
        try (FileReader reader = new FileReader(path.toString())) {
            JSONTokener tokener = new JSONTokener(reader);
            return (T)tokener.nextValue();
        } catch (JSONException ex) {
            throw new RuntimeException("JSON error.");
        } catch (FileNotFoundException ex) {
            throw new RuntimeException("File not found.");
        } catch (IOException ex) {
            throw new RuntimeException("I/O error.");
        }
    }
    public static void writeFile(JSONObject o, Path path) {
        try (FileWriter writer = new FileWriter(path.toString())) {
            writer.append(o.toString(4));
            writer.flush();
        } catch (IOException ex) {
            throw new RuntimeException("I/O error.");
        }
    }
    public static void writeFile(JSONArray o, Path path) {
        try (FileWriter writer = new FileWriter(path.toString())) {
            writer.append(o.toString(4));
            writer.flush();
        } catch (IOException ex) {
            throw new RuntimeException("I/O error.");
        }
    }

    public JSONModel(Path folder) {
        this.folder = folder;
        this.credentials  = readFile(path_credentials  = folder.resolve("credentials.json"));
        this.identities   = readFile(path_identities   = folder.resolve("identities.json"));
        this.insegnamenti = readFile(path_insegnamenti = folder.resolve("insegnamenti.json"));
        this.lauree       = readFile(path_lauree       = folder.resolve("lauree.json"));
        this.chat_recenti = readFile(path_chat_recenti = folder.resolve("chat/recents.json"));
        this.chat         = readFile(path_chat         = folder.resolve("chat/chat.json"));
    }


    private Path getPathLezioni(String... id) {
        return folder.resolve(String.format("lezioni/%s.json", (Object[])id));
    }
    private Path getPathLezioni(Insegnamento ins) {
        return getPathLezioni(ins.id);
    }

    private Path getPathMateriali(String... id) {
        return folder.resolve(String.format("materiali/%s/%s.json", (Object[])id));
    }
    private Path getPathMateriali(Lezione lez) {
        return getPathMateriali(lez.insegnamento.id, lez.id);
    }

    private Path getPathCompiti(String... id) {
        return folder.resolve(String.format("compiti/%s/%s.json", (Object[])id));
    }
    private Path getPathCompiti(Lezione lez) {
        return getPathCompiti(lez.insegnamento.id, lez.id);
    }

    private Path getPathConsegne(String... id) {
        return folder.resolve(String.format("consegne/%s/%s/%s.json", (Object[])id));
    }
    private Path getPathConsegne(Compito com) {
        return getPathConsegne(com.lezione.insegnamento.id, com.lezione.id, com.id);
    }

    private Path getPathFile(String id) {
        return folder.resolve(String.format("files/%s", id)).toFile().listFiles()[0].toPath();
    }
    private Path getPathNewFile(String id, String name) {
        return folder.resolve(String.format("files/%s/%s", id, name));
    }

    private Path getPathChatMessages(String id) {
        return folder.resolve(String.format("chat/messages/%s.json", id));
    }


    private boolean makeParentAndFile(File f) {
        File p = f.getParentFile();
        if (!p.exists()) {
            if (!p.mkdir())
                throw new RuntimeException("Failed to create directory " + p.toPath());
        }
        boolean ret = !f.exists();
        if (ret) {
            try {
                f.createNewFile();
            }
            catch (IOException ex) { throw new RuntimeException(); }
        }
        return ret; // created new file
    }
    private void saveJSON(JSONObject... objs) {
        for (JSONObject o : objs) {
            if      (o == credentials)  writeFile(o, path_credentials);
            else if (o == identities)   writeFile(o, path_identities);
            else if (o == insegnamenti) writeFile(o, path_insegnamenti);
        }
    }
    public Pair<Boolean, Object> custClassDeserialize(JSONObject o, String name, Class<?> c) {
        boolean ok = true;
        Object r = null;

        if (c.equals(classiPersone.Docente.class)) {
            r = getIdentity(o.getString(name));
            if (!(r instanceof classiPersone.Docente))
                throw new DataIsNotCoherentException();
        }
        else if (c.equals(classiPersone.Studente.class)) {
            r = getIdentity(o.getString(name));
            if (!(r instanceof classiPersone.Studente))
                throw new DataIsNotCoherentException();
        }
        else if (c.equals(classiPersone.Persona.class)) {
            r = getIdentity(o.getString(name));
        }
        else if (c.equals(classiConferencePlatform.DummyPrenotazione.class)) {
            r = o.optJSONObject(name);
            if (r != null)
                r = deserializeObject((JSONObject)r, c, null, this::custClassDeserialize);
        }
        else
            ok = false;

        return new Pair<>(ok, r);
    }
    public Pair<Boolean, Pair<String, Object>> custClassSerialize(Object o, String name, Class<?> c) {
        if (c.equals(classiConferencePlatform.DummyPrenotazione.class))
            return tuple(true, name, serializeObject(o, this::custClassSerialize));
        return tuple(false, null);
    }
    public static <T> Pair<String, T> traverse(JSONObject db1, JSONObject db2, Identifiable<String> from, String link) {
        String key = db1.getJSONObject(from.id).getString(link);
        return tuple(key, (T)db2.get(key));
    }

    @Override
    public Persona getIdentity(String matricola) {
        JSONObject o = this.identities.getJSONObject(matricola);
        TipoPersona t = o.getEnum(TipoPersona.class, "tipo");
        Persona p = null;
        ICustKeyDeserializer<String> f = keyFetch("id", matricola);
        switch (t) {
            case STUDENTE: p = deserializeObject(o, Studente.class, f, this::custClassDeserialize); break;
            case DOCENTE:  p = deserializeObject(o, Docente.class, f, this::custClassDeserialize); break;
            default: throw new NoClassDefFoundError(t.name());
        }
        return p;
    }
    @Override
    public void aggiornaAccesso(String matricola) {
        JSONObject o = this.identities.getJSONObject(matricola);
        Object ser = trySerializeField(LocalDateTime.now(), null, LocalDateTime.class).snd.snd;
        o.put("ultimaVisita", ser);
        writeFile(this.identities, path_identities);
    }

    @Override
    public Persona login(String matricola, String password) throws InvalidCredentialsException {
        if (!this.credentials.has(matricola) || !this.credentials.getString(matricola).equals(password))
            throw new InvalidCredentialsException();
        aggiornaAccesso(matricola);
        return getIdentity(matricola);
    }
    @Override
    public Laurea getLaurea(Studente s) {
        Pair<String, JSONObject> v = traverse(identities, lauree, s, "laurea");
        return deserializeObject(v.snd, Laurea.class, keyFetch(v.fst), this::custClassDeserialize);
    }
    @Override
    public List<Insegnamento> getInsegnamenti(Laurea l) {
        List<Insegnamento> ret = new ArrayList<>();
        JSONObject v = lauree.getJSONObject(l.id);
        JSONArray l_insegnamenti = v.getJSONArray("insegnamenti");
        for (Object ik : l_insegnamenti) {
            JSONObject i = insegnamenti.getJSONObject((String)ik);
            Insegnamento ins = deserializeObject(i, Insegnamento.class, keyFetch(ik), this::custClassDeserialize);
            ret.add(ins);
        }
        return ret;
    }
    @Override
    public List<Insegnamento> getInsegnamenti(Persona p, boolean coinvolto) {
        if (p == null)
            throw new IllegalArgumentException("Using null object.");
        List<Insegnamento> ret = new ArrayList<>();
        if (p instanceof Studente) {
            Pair<String, JSONObject> v = traverse(identities, lauree, p, "laurea");
            JSONArray l_insegnamenti = v.snd.getJSONArray("insegnamenti");
            for (Object ik : l_insegnamenti) {
                JSONObject i = insegnamenti.getJSONObject((String)ik);
                JSONArray matricole = i.getJSONArray("iscritti");
                boolean found = false;
                for (Object s : matricole) {
                    found = p.id.equals(s);
                    if (found)
                        break;
                }
                if (found != coinvolto)
                    continue;
                Insegnamento ins = deserializeObject(i, Insegnamento.class, keyFetch(ik), this::custClassDeserialize);
                ret.add(ins);
            }
        } else if (p instanceof Docente) {
            String ikey = "docenti";
            for (String k : insegnamenti.keySet()) {
                JSONObject v = insegnamenti.getJSONObject(k);
                JSONArray matricole = v.getJSONArray(ikey);
                boolean found = false;
                for (Object s : matricole) {
                    found = p.id.equals(s);
                    if (found)
                        break;
                }
                if (found != coinvolto)
                    continue;
                Insegnamento ins = deserializeObject(v, Insegnamento.class, keyFetch(k), this::custClassDeserialize);
                ret.add(ins);
            }
        }
        return ret;
    }
    @Override
    public Set<Laurea> getCorsiLaureaInsegnamento(Insegnamento ins) {
        Set<Laurea> ret = new HashSet<>();
        for (String k : lauree.keySet()) {
            JSONObject laurea = lauree.getJSONObject(k);
            JSONArray l_insegnamenti = laurea.getJSONArray("insegnamenti");
            boolean found = false;
            for (Object ik : l_insegnamenti) {
                found = ins.id.equals(ik);
                if (found)
                    break;
            }
            if (!found)
                continue;
            ret.add(deserializeObject(laurea, Laurea.class, keyFetch(k), this::custClassDeserialize));
        }
        return ret;
    }

    @Override
    public Set<Studente> getIscritti(Insegnamento ins) {
        JSONObject ji = (JSONObject)insegnamenti.get(ins.id);
        JSONArray ja = (JSONArray)ji.get("iscritti");
        Set<Studente> ret = new HashSet<>();
        ja.forEach((sid) -> ret.add((Studente)getIdentity((String)sid)));
        return ret;
    }
    @Override
    public void modificaIscrizione(Studente s, Insegnamento ins, DataOperation op) {
        if (op == DataOperation.UPDATE)
            return;
        boolean iscrivi = (op == DataOperation.ADD);
        JSONObject ji = (JSONObject)insegnamenti.get(ins.id);
        JSONArray ja = (JSONArray)ji.get("iscritti");
        int i = 0, index = -1;
        for (Object m : ja) {
            if (s.id.equals(m)) {
                index = i;
                break;
            }
            i++;
        }
        if (iscrivi && index < 0)
            ja.put(s.id);
        else if (!iscrivi && index >= 0)
            ja.remove(index);
        saveJSON(insegnamenti);
    }

    @Override
    public boolean isDocente(Insegnamento ins, Docente d) {
        JSONObject ji = (JSONObject)insegnamenti.get(ins.id);
        JSONArray ja = (JSONArray)ji.get("docenti");
        boolean found = false;
        for (Object doc : ja) {
            found = d.id.equals(doc);
            if (found)
                break;
        }
        return found;
    }
    @Override
    public Set<Docente> getDocenti(Insegnamento ins) {
        JSONObject ji = (JSONObject)insegnamenti.get(ins.id);
        JSONArray ja = (JSONArray)ji.get("docenti");
        Set<Docente> ret = new HashSet<>();
        ja.forEach((sid) -> ret.add((Docente)getIdentity((String)sid)));
        return ret;
    }
    @Override
    public List<Lezione> getLezioni(Insegnamento ins) {
        Path p = getPathLezioni(ins);
        List<Lezione> ret = new ArrayList<>();
        if (!p.toFile().isFile())
            return ret;
        JSONObject jlez = readFile(p);
        for (String k : jlez.keySet()) {
            JSONObject v = jlez.getJSONObject(k);
            TipoLezione t = v.getEnum(TipoLezione.class, "tipo");
            Lezione lez = null;
            ICustKeyDeserializer<Object> f = keyFetch(k, (s) -> {
                if (s.equals("insegnamento"))
                    return ins;
                return null;
            });
            switch (t) {
                case PRESENZA:       lez = deserializeObject(v, LezionePresenza.class, f, this::custClassDeserialize); break;
                case STREAMING:      lez = deserializeObject(v, LezioneStreaming.class, f, this::custClassDeserialize); break;
                case SOLO_MATERIALE: lez = deserializeObject(v, LezioneSoloMateriale.class, f, this::custClassDeserialize); break;
                default: throw new NoClassDefFoundError(t.name());
            }
            ret.add(lez);
        }
        return ret;
    }
    @Override
    public void modificaLezione(Lezione l, DataOperation op, Changes<Materiale> em, Changes<Compito> ea) {
        Path pl = getPathLezioni(l.insegnamento);
        File f = pl.toFile();
        ICustClassSerializer cust = (obj, name, c) -> {
            if (name.equals("id") || name.equals("insegnamento"))
                return tuple(true, null, null); //handle, delete keys
            if (name.equals("docente"))
                return tuple(true, name, ((Docente)obj).id);
            return custClassSerialize(obj, name, c);
        };
        JSONObject jlez, serial;
        switch (op) {
            case ADD:
                jlez = makeParentAndFile(f)? new JSONObject() : readFile(pl);
                serial = JSONReflect.serializeObject(l, cust);
                jlez.put(l.id, serial);
                writeFile(jlez, pl);
                if (em != null)
                    for (Materiale m : em.changes.get(op)) {
                        m.lezione = l;
                        modificaMateriale(m, op);
                    }
                if (ea != null)
                    for (Compito c : ea.changes.get(op)) {
                        c.lezione = l;
                        modificaCompito(c, op);
                    }
                break;
            case UPDATE:
                assertFileExists(pl);
                jlez = readFile(pl);
                serial = JSONReflect.serializeObject(l, cust);
                jlez.remove(l.id);
                jlez.put(l.id, serial);
                writeFile(jlez, pl);
                if (em != null)
                    for (DataOperation op2 : em.changes.keySet()) {
                        for (Materiale m : em.changes.get(op2))
                            modificaMateriale(m, op2);
                    }
                if (ea != null)
                    for (DataOperation op2 : ea.changes.keySet()) {
                        for (Compito c : ea.changes.get(op2))
                            modificaCompito(c, op2);
                    }
                break;
            case DELETE:
                eliminaMateriali(l.insegnamento.id, l.id);
                eliminaCompiti(l.insegnamento.id, l.id);
                if (!f.exists()) return;
                jlez = readFile(pl);
                jlez.remove(l.id);
                writeFile(jlez, pl);
                break;
        }
    }

    @Override
    public Set<Studente> getPartecipanti(Lezione l) {
        Path p = getPathLezioni(l.insegnamento);
        assertFileExists(p);
        JSONObject jlez_all = readFile(p);
        JSONObject jlez = jlez_all.getJSONObject(l.id);
        JSONArray part = jlez.optJSONArray("partecipanti");
        Set<Studente> ret = new TreeSet<>();
        if (part == null)
            return ret;
        for (Object o : part)
            ret.add((Studente)getIdentity((String)o));
        return ret;
    }
    @Override
    public void aggiungiPartecipante(Lezione l, Studente s) {
        Path p = getPathLezioni(l.insegnamento);
        assertFileExists(p);
        JSONObject jlez_all = readFile(p);
        JSONObject jlez = jlez_all.getJSONObject(l.id);
        JSONArray part = jlez.optJSONArray("partecipanti");
        if (part == null) {
            part = new JSONArray();
            jlez.put("partecipanti", part);
        }
        part.put(s.id);
        writeFile(jlez_all, p);
    }

    @Override
    public List<Materiale> getMateriali(Lezione l) {
        Path p = getPathMateriali(l);
        List<Materiale> ret = new ArrayList<>();
        if (!p.toFile().exists())
            return ret;
        JSONObject jmat = readFile(p);
        for (String k : jmat.keySet()) {
            JSONObject v = jmat.getJSONObject(k);
            ICustKeyDeserializer<Object> f = keyFetch(k, (s) -> {
                if (s.equals("lezione"))
                    return l;
                if (s.equals("contenuti"))
                    return getListaFiles(v.getJSONArray(s).toList().stream().toArray(String[]::new));
                return null;
            });
            Materiale mat = deserializeObject(v, Materiale.class, f, this::custClassDeserialize);
            ret.add(mat);
        }
        return ret;
    }
    @Override
    public void modificaMateriale(Materiale m, DataOperation op) {
        Path pm = getPathMateriali(m.lezione);
        File f = pm.toFile();
        final List<String> contenuti = new ArrayList<>();
        ICustClassSerializer cust = (obj, name, c) -> {
            if (name.equals("id") || name.equals("lezione"))
                return tuple(true, null, null); //handle, delete keys
            if (name.equals("contenuti"))
                return tuple(true, name, contenuti);
            return custClassSerialize(obj, name, c);
        };
        JSONObject jmat, serial;
        switch (op) {
            case ADD:
                jmat = makeParentAndFile(f)? new JSONObject() : readFile(pm);
                if (jmat.has(m.id))
                    throw new DataIsNotCoherentException();
                for (IFile iif : m.contenuti) {
                    if (iif instanceof FileRemoto)
                        throw new DataIsNotCoherentException();
                }
                for (IFile iif : m.contenuti)
                    contenuti.add(aggiungiFile((FileLocale)iif));
                serial = JSONReflect.serializeObject(m, cust);
                jmat.put(m.id, serial);
                writeFile(jmat, pm);
                break;
            case UPDATE:
                assertFileExists(pm);
                jmat = readFile(pm);
                for (Object id : jmat.getJSONObject(m.id).getJSONArray("contenuti")) {
                    boolean keep = m.contenuti.stream()
                        .anyMatch(iif -> (iif instanceof FileRemoto) && ((FileRemoto)iif).id.equals(id));
                    if (!keep)
                        rimuoviFile((String)id);
                    else
                        contenuti.add((String)id);
                }
                m.contenuti.stream()
                    .filter(iif -> iif instanceof FileLocale)
                    .forEach(iif -> contenuti.add(aggiungiFile((FileLocale)iif)));
                serial = JSONReflect.serializeObject(m, cust);
                jmat.remove(m.id);
                jmat.put(m.id, serial);
                writeFile(jmat, pm);
                break;
            case DELETE:
                jmat = readFile(pm);
                for (Object id : jmat.getJSONObject(m.id).getJSONArray("contenuti"))
                    rimuoviFile((String)id);
                jmat.remove(m.id);
                writeFile(jmat, pm);
                break;
        }
    }
    @Override
    public void eliminaMateriali(String id_ins, String id_lez) {
        File f = getPathMateriali(id_ins, id_lez).toFile();
        JSONObject jdel;
        if (f.exists()) {
            jdel = readFile(f.toPath());
            for (String k : jdel.keySet()) {
                for (Object id : jdel.getJSONObject(k).getJSONArray("contenuti"))
                    rimuoviFile((String)id);
            }
            f.delete();
        }
    }

    @Override
    public List<Compito> getCompiti(Lezione l) {
        Path p = getPathCompiti(l);
        List<Compito> ret = new ArrayList<>();
        if (!p.toFile().exists())
            return ret;
        JSONObject jcom = readFile(p);
        for (String k : jcom.keySet()) {
            JSONObject v = jcom.getJSONObject(k);
            ICustKeyDeserializer<Object> f = keyFetch(k, (s) -> {
                if (s.equals("lezione"))
                    return l;
                return null;
            });
            Compito com = deserializeObject(v, Compito.class, f, this::custClassDeserialize);
            ret.add(com);
        }
        return ret;
    }
    @Override
    public void modificaCompito(Compito c, DataOperation op) {
        Path pc = getPathCompiti(c.lezione);
        File f = pc.toFile();
        ICustClassSerializer cust = (obj, name, cl) -> {
            if (name.equals("id") || name.equals("lezione"))
                return tuple(true, null, null); //handle, delete keys
            return custClassSerialize(obj, name, cl);
        };
        JSONObject jcom, serial;
        c.ultimaModifica = LocalDateTime.now();
        switch (op) {
            case ADD:
                jcom = makeParentAndFile(f)? new JSONObject() : readFile(pc);
                if (jcom.has(c.id))
                    throw new DataIsNotCoherentException();
                serial = JSONReflect.serializeObject(c, cust);
                jcom.put(c.id, serial);
                writeFile(jcom, pc);
                break;
            case UPDATE:
                assertFileExists(pc);
                jcom = readFile(pc);
                serial = JSONReflect.serializeObject(c, cust);
                jcom.remove(c.id);
                jcom.put(c.id, serial);
                writeFile(jcom, pc);
                break;
            case DELETE:
                eliminaConsegne(c.lezione.insegnamento.id, c.lezione.id, c.id);
                if (!f.exists()) return;
                jcom = readFile(pc);
                jcom.remove(c.id);
                writeFile(jcom, pc);
                break;
        }
    }
    @Override
    public void eliminaCompiti(String id_ins, String id_lez) {
        File f = getPathCompiti(id_ins, id_lez).toFile();
        JSONObject jdel;
        if (f.exists()) {
            jdel = readFile(f.toPath());
            for (String k : jdel.keySet())
                eliminaConsegne(id_ins, id_lez, k);
            f.delete();
        }
    }

    private Consegna getConsegna(Compito c, JSONObject jcon, Studente st) {
        JSONObject v = jcon.optJSONObject(st.id);
        if (v == null)
            return null;
        ICustKeyDeserializer<Object> f = keyFetch(st.id, (s) -> {
            if (s.equals("compito"))
                return c;
            if (s.equals("studente"))
                return st;
            if (s.equals("caricati"))
                return getListaFiles(v.getJSONArray(s).toList().stream().toArray(String[]::new));
            return null;
        });
        return deserializeObject(v, Consegna.class, f, this::custClassDeserialize);
    }
    @Override
    public Consegna getConsegna(Compito c, Studente s) {
        Path p = getPathConsegne(c);
        if (!p.toFile().exists())
            return null;
        JSONObject jcon = readFile(p);
        return getConsegna(c, jcon, s);
    }
    @Override
    public Map<Studente, Consegna> getConsegneCaricate(Compito c) {
        Path p = getPathConsegne(c);
        Map<Studente, Consegna> ret = new HashMap<>();
        if (!p.toFile().exists())
            return ret;
        JSONObject jcon = readFile(p);
        for (String k : jcon.keySet()) {
            Studente st = (Studente)getIdentity(k);
            ret.put(st, getConsegna(c, jcon, st));
        }
        return ret;
    }
    @Override
    public void modificaConsegna(Consegna c, DataOperation op) {
        Path pc = getPathConsegne(c.compito);
        File f = pc.toFile();
        final List<String> caricati = new ArrayList<>();
        ICustClassSerializer cust = (obj, name, cl) -> {
            if (name.equals("id") || name.equals("compito") || name.equals("studente"))
                return tuple(true, null, null); //handle, delete keys
            if (name.equals("caricati"))
                return tuple(true, name, caricati);
            return custClassSerialize(obj, name, cl);
        };
        JSONObject jcon, serial;
        switch (op) {
            case ADD:
                jcon = makeParentAndFile(f)? new JSONObject() : readFile(pc);
                if (jcon.has(c.studente.id))
                    throw new DataIsNotCoherentException();
                for (IFile iif : c.caricati) {
                    if (iif instanceof FileRemoto)
                        throw new DataIsNotCoherentException();
                }
                for (IFile iif : c.caricati)
                    caricati.add(aggiungiFile((FileLocale)iif));
                serial = JSONReflect.serializeObject(c, cust);
                jcon.put(c.studente.id, serial);
                writeFile(jcon, pc);
                break;
            case UPDATE:
                assertFileExists(pc);
                jcon = readFile(pc);
                for (Object id : jcon.getJSONObject(c.studente.id).getJSONArray("caricati")) {
                    boolean keep = c.caricati.stream()
                        .anyMatch(iif -> (iif instanceof FileRemoto) && ((FileRemoto)iif).id.equals(id));
                    if (!keep)
                        rimuoviFile((String)id);
                    else
                        caricati.add((String)id);
                }
                c.caricati.stream()
                    .filter(iif -> iif instanceof FileLocale)
                    .forEach(iif -> caricati.add(aggiungiFile((FileLocale)iif)));
                serial = JSONReflect.serializeObject(c, cust);
                jcon.remove(c.studente.id);
                jcon.put(c.studente.id, serial);
                writeFile(jcon, pc);
                break;
            case DELETE:
                jcon = readFile(pc);
                for (Object id : jcon.getJSONObject(c.studente.id).getJSONArray("caricati"))
                    rimuoviFile((String)id);
                jcon.remove(c.studente.id);
                writeFile(jcon, pc);
                break;
        }
    }
    @Override
    public void eliminaConsegne(String id_ins, String id_lez, String id_com) {
        File f = getPathConsegne(id_ins, id_lez, id_com).toFile();
        JSONObject jdel;
        if (f.exists()) {
            jdel = readFile(f.toPath());
            for (String k : jdel.keySet()) {
                for (Object id : jdel.getJSONObject(k).getJSONArray("caricati"))
                    rimuoviFile((String)id);
            }
            f.delete();
        }
    }

    @Override
    public List<FileRemoto> getListaFiles(String... ids) {
        List<FileRemoto> ret = new ArrayList<>();
        for (String id : ids) {
            Path p = getPathFile(id);
            File f = p.toFile();
            FileRemoto fo = null;
            try {
                fo = new FileRemoto(
                    id, f.getName(),
                    Files.probeContentType(p),
                    f.length()
                );
            } catch (IOException ex) { throw new RuntimeException(); }
            ret.add(fo);
        }
        return ret;
    }
    @Override
    public void leggiContenutoFile(String id, Consumer<InputStream> c) {
        Path p = getPathFile(id);
        try (InputStream str = new FileInputStream(p.toFile())) {
            c.accept(str);
        }
        catch (FileNotFoundException ex) { throw new RuntimeException(); }
        catch (IOException ex) { throw new RuntimeException(); }
    }
    @Override
    public void scriviContenutoFile(String id, InputStream s) {
        Path p = getPathFile(id);
        try {
            Files.copy(s, p, StandardCopyOption.REPLACE_EXISTING);
        }
        catch (FileNotFoundException ex) { throw new RuntimeException(); }
        catch (IOException ex) { throw new RuntimeException(); }
    }
    @Override
    public String aggiungiFile(FileLocale f) {
        String id = genid();
        try (FileInputStream is = new FileInputStream(f.file)) {
            File nf = getPathNewFile(id, f.file.getName()).toFile();
            makeParentAndFile(nf);
            scriviContenutoFile(id, is);
        }
        catch (FileNotFoundException ex) { throw new RuntimeException(); }
        catch (IOException ex) { throw new RuntimeException(); }
        return id;
    }
    @Override
    public void rimuoviFile(String id) {
        File f = getPathFile(id).toFile();
        File d = f.getParentFile();
        f.delete();
        d.delete();
    }

    @Override
    public Chat apriChat(String id) {
        JSONObject obj = chat.optJSONObject(id);
        if (obj == null)
            return null;
        TipoChat t = obj.getEnum(TipoChat.class, "tipo");
        Chat ret;
        ICustKeyDeserializer<Object> f = keyFetch(id, (s) -> {
            if (s.equals("partecipanti")) {
                Set<Persona> sret = new HashSet<>();
                for (Object pid : obj.getJSONArray(s))
                    sret.add(getIdentity((String)pid));
                return sret;
            }
            return null;
        });
        switch (t) {
            case PRIVATO:   ret = deserializeObject(obj, ChatPrivata.class, f, this::custClassDeserialize); break;
            case BROADCAST: ret = deserializeObject(obj, ChatBroadcast.class, f, this::custClassDeserialize); break;
            default: throw new NoClassDefFoundError(t.name());
        }
        return ret;
    }
    @Override
    public List<Chat> getRecenti(Persona p) {
        JSONArray rec = chat_recenti.optJSONArray(p.id);
        List<Chat> ret = new ArrayList<>();
        if (rec != null)
            for (Object id : rec)
                ret.add(apriChat((String)id));
        return ret;
    }
    @Override
    public void setRecenti(Persona p, List<Chat> lst) {
        lst.forEach(c -> salvaChat(c));
        chat_recenti.put(p.id, new JSONArray(lst.stream()
            .map(c -> c.id)
            .collect(Collectors.toList())
        ));
        writeFile(chat_recenti, path_chat_recenti);
    }
    @Override
    public Chat apriChatPrivata(Persona p1, Persona p2) {
        if (p1.equals(p2))
            return null;
        List<String> lst = List.of(p1.id, p2.id);
        String concat = lst.stream().sorted().collect(Collectors.joining());
        String id = b64e.encodeToString(concat.getBytes());
        Chat c = apriChat(id);
        if (c == null)
            c = new ChatPrivata(id, Set.of(p1, p2));
        return c;
    }
    @Override
    public void salvaChat(Chat c) {
        ICustClassSerializer cust = (obj, name, cl) -> {
            if (name.equals("id"))
                return tuple(true, null, null); //handle, delete keys
            if (name.equals("partecipanti")) {
                List<String> ids = ((Set<Persona>)obj).stream()
                    .map(p -> p.id)
                    .collect(Collectors.toList());
                JSONArray part = new JSONArray(ids);
                return tuple(true, name, part);
            }
            return custClassSerialize(obj, name, cl);
        };
        chat.put(c.id, serializeObject(c, cust));
        writeFile(chat, path_chat);
    }
    @Override
    public List<Messaggio> getMessaggi(Chat c) {
        Path p = getPathChatMessages(c.id);
        List<Messaggio> ret = new ArrayList<>();
        if (!p.toFile().exists())
            return ret;
        JSONArray jmsgs = readFile(p);
        for (Object v : jmsgs) {
            Messaggio msg = deserializeObject((JSONObject)v, Messaggio.class, null, this::custClassDeserialize);
            ret.add(msg);
        }
        return ret;
    }
    @Override
    public void mandaMessaggio(Chat c, Messaggio m, boolean salva_recenti) {
        Path p = getPathChatMessages(c.id);
        JSONArray msgs;
        if (p.toFile().exists())
            msgs = readFile(p);
        else {
            salvaChat(c);
            msgs = new JSONArray();
        }
        ICustClassSerializer f = (o, name, cl) -> {
            if (name.equals("mittente"))
                return tuple(true, name, ((Persona)o).id);
            return tuple(false, null);
        };
        msgs.put(serializeObject(m, f));
        writeFile(msgs, p);
        if (c.tipo == TipoChat.BROADCAST) {
            for (Persona p2 : c.partecipanti) {
                Chat priv = apriChatPrivata(p2, m.mittente);
                if (priv == null)
                    continue;
                mandaMessaggio(priv, m, false);
            }
        } else {
            for (Persona pe : c.partecipanti) {
                JSONArray rec = chat_recenti.optJSONArray(pe.id);
                if (rec == null)
                    rec = new JSONArray(List.of(c.id));
                else {
                    int i = 0;
                    for (Object cid2 : rec) {
                        if (cid2.equals(c.id))
                            break;
                        i++;
                    }
                    if (i > 0) {
                        if (i < rec.length())
                            rec.remove(i);
                        rec.put(0, c.id);
                    }
                }
                chat_recenti.put(pe.id, rec);
            }
        }
        if (salva_recenti)
            writeFile(chat_recenti, path_chat_recenti);
    }
}
