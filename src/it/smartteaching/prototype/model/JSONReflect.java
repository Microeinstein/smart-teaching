package it.smartteaching.prototype.model;

import it.smartteaching.prototype.Core;
import it.smartteaching.prototype.Core.Pair;
import java.lang.reflect.*;
import java.time.*;
import java.util.*;
import java.util.logging.*;
import java.util.stream.Collectors;
import org.json.*;

// COMPILE WITH -parameters OPTION

public class JSONReflect {
    public static interface ICustKeyDeserializer<T> {
        T apply(String key);
    }
    public static interface ICustClassDeserializer {
        Pair<Boolean, Object> apply(JSONObject o, String name, Class<?> c);
    }
    public static interface ICustClassSerializer<T> {
        Pair<Boolean, Pair<String, Object>> apply(Object o, String name, Class<?> c);
    }
    public static ICustKeyDeserializer keyFetch(String key, Object value, ICustKeyDeserializer<?> other) {
        return (s) -> {
            if (s.equals(key))
                return value;
            if (other != null)
                return other.apply(s);
            return null;
        };
    }
    public static ICustKeyDeserializer keyFetch(String key, Object value) {
        return keyFetch(key, value, null);
    }
    public static ICustKeyDeserializer keyFetch(Object value, ICustKeyDeserializer<?> other) {
        return keyFetch("id", value, other);
    }
    public static ICustKeyDeserializer keyFetch(Object value) {
        return keyFetch(value, null);
    }

    public static Pair<Boolean, Object> tryDeserializeField(JSONObject o, String name, Class<?> c) {
        boolean ok = true;
        Object r = null;

        if      (c.equals(int.class))           r = o.getInt(name);
        else if (c.equals(long.class))          r = o.getLong(name);
        else if (c.equals(float.class))         r = o.getFloat(name);
        else if (c.equals(double.class))        r = o.getDouble(name);
        else if (c.equals(boolean.class))       r = o.getBoolean(name);
        else if (c.equals(String.class))        r = o.getString(name);
        else if (c.isEnum())                    r = o.getEnum((Class<Enum>)c, name);
        else if (c.equals(LocalDateTime.class)) r = LocalDateTime.ofInstant(
            Instant.ofEpochSecond(o.getLong(name)),
            ZoneId.systemDefault()
        );
        else if (c.equals(List.class))          r = o.getJSONArray(name).toList();
        else
            ok = false;

        return new Pair<>(ok, r);
    }
    public static Object deserializeField(JSONObject o, String name, Class<?> c, ICustClassDeserializer fbclass) {
        Pair<Boolean, Object> p;
        p = tryDeserializeField(o, name, c);
        if (!p.fst && fbclass != null)
            p = fbclass.apply(o, name, c);
        if (!p.fst)
            throw new RuntimeException("Unknown field type.");
        return p.snd;
    }
    public static <T> T deserializeObject(JSONObject o, Class<T> c, ICustKeyDeserializer custkey, ICustClassDeserializer custclass) {
        Constructor<?> constr = c.getConstructors()[0];
        List<Parameter> params = List.of(constr.getParameters());
        ArrayList<Object> args = new ArrayList<>();

        //obtain constructor arguments
        for (Parameter p : params) {
            if (p.isImplicit())
                throw new RuntimeException("Nested classes are not supported.");
            String name = p.getName();
            Class<?> t = p.getType();
            if (t.isArray())
                throw new RuntimeException("Arrays are not supported.");
            Object v = null;
            if (custkey != null)
                v = custkey.apply(name);
            if (v == null && o.has(name))
                v = deserializeField(o, name, t, custclass);
            //if (v == null)
            //    throw new Core.MissingRequiredFieldException();
            args.add(v);
        }

        //create instance
        T ret = null;
        try {
            ret = (T)constr.newInstance(args.toArray());
        } catch (InstantiationException |
                 IllegalAccessException |
               IllegalArgumentException |
              InvocationTargetException ex) {
            Logger.getLogger(JSONModel.class.getName()).log(Level.SEVERE, null, ex);
        }

        //obtain fields that probably aren't assigned from the constructor
        List<String> paramNames = params.stream()
            .map(Parameter::getName)
            .collect(Collectors.toList());
        for (Field f : c.getFields()) {
            String name = f.getName();
            if (!o.has(name))
                continue;
            int mod = f.getModifiers();
            if (!Modifier.isPublic(mod) ||
                Modifier.isStatic(mod) ||
                Modifier.isFinal(mod) ||
                paramNames.contains(name)) // must be public non-static
                continue;
            try {
                f.set(ret, deserializeField(o, name, f.getType(), custclass));
            } catch (IllegalArgumentException |
                     IllegalAccessException ex) {
            }
        }

        return ret;
    }

    private static List<Class<?>> BASIC_SERIALIZABLE = List.of(
        int.class, long.class, float.class, double.class, boolean.class, String.class
    );
    public static Pair<Boolean, Pair<String, Object>> trySerializeField(Object o, String name, Class<?> c) {
        boolean ok = true;
        String k = name;
        Object r = null;

        if (BASIC_SERIALIZABLE.contains(c))
            r = o;
        else if (c.isEnum())
            r = ((Enum)o).name();
        else if (c.equals(LocalDateTime.class))
            r = ((LocalDateTime)o).toInstant(
                OffsetDateTime.now().getOffset()
            ).getEpochSecond();
        else
            ok = false;

        return new Pair<>(ok, new Pair<>(k, r));
    }
    public static void serializeField(JSONObject jo, Object o, String name, Class<?> c, ICustClassSerializer custkey) {
        Pair<Boolean, Pair<String, Object>> p = null;
        if (custkey != null) {
            p = custkey.apply(o, name, c);
            if (!p.fst)
                p = trySerializeField(o, name, c);
        } else
            p = trySerializeField(o, name, c);
        if (!p.fst)
            throw new RuntimeException("Unknown field type.");
        String key = p.snd.fst;
        if (key == null) //intention: skip this field
            return;
        jo.put(key, p.snd.snd);
    }
    public static <T> JSONObject serializeObject(T o, ICustClassSerializer custkey) {
        Class<T> c = (Class<T>)o.getClass();
        Constructor<?> constr = c.getConstructors()[0];
        List<Parameter> params = List.of(constr.getParameters());
        ArrayList<Object> args = new ArrayList<>();
        JSONObject r = new JSONObject();

        //convert fields that probably aren't assigned from the constructor
        List<String> paramNames = params.stream()
            .map(Parameter::getName)
            .collect(Collectors.toList());
        for (Field f : c.getFields()) {
            String name = f.getName();
            int mod = f.getModifiers();
            if (!Modifier.isPublic(mod) ||
                Modifier.isStatic(mod) ||
                (paramNames.contains(name) && Modifier.isFinal(mod))) // must be public non-static
                continue;
            try {
                serializeField(r, f.get(o), name, f.getType(), custkey);
            } catch (IllegalArgumentException |
                     IllegalAccessException ex) {
            }
        }

        //convert remaining fields matching constructor arguments
        for (Parameter p : params) {
            if (p.isImplicit())
                throw new RuntimeException("Nested classes are not supported.");
            String name = p.getName();
            Class<?> t = p.getType();
            if (t.isArray())
                throw new RuntimeException("Arrays are not supported.");
            try {
                serializeField(r, c.getField(name).get(o), name, t, custkey);
            } catch (NoSuchFieldException |
                     SecurityException |
                     IllegalAccessException ex) {
                throw new Core.MissingRequiredFieldException();
            }
        }

        return r;
    }
}
