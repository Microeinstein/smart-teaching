package it.smartteaching.prototype.model;

import static it.smartteaching.prototype.Core.*;
import java.nio.ByteBuffer;
import java.time.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Random;

public interface classiConferencePlatform {
    public static interface IPrenotazione {
        LocalDateTime getDate();
        String getLink();
        default void free() {
            IConferencePlatform.CONFPLATFORM.free(this);
        }
    }

    public static interface IConferencePlatform {
        public static final DummyConferencePlatform CONFPLATFORM = new DummyConferencePlatform();
        IPrenotazione book(LocalDateTime date);
        void free(IPrenotazione p);
    }

    public static class DummyPrenotazione implements IPrenotazione {
        public final String link;
        public final LocalDateTime date;

        public DummyPrenotazione(String link, LocalDateTime date) {
            this.link = link;
            this.date = date;
        }

        @Override
        public String getLink() {
            return this.link;
        }
        @Override
        public LocalDateTime getDate() {
            return this.date;
        }
    }

    public static class DummyConferencePlatform implements IConferencePlatform {
        private final String ytbase = "https://www.youtube.com/watch?v=";
        private final Random rand = new Random();

        @Override
        public IPrenotazione book(LocalDateTime date) {
            if (rand.nextFloat() <= 1./3)
                return null;
            // should be unsigned
            long ytnum = ThreadLocalRandom.current().nextLong(Long.MAX_VALUE);
            ByteBuffer bb = ByteBuffer.allocate(Long.BYTES);
            bb.putLong(ytnum);
            bb.flip(); // big-endian
            String txt = b64e.encodeToString(bb.array())
                .replace('+','-')
                .replace('/','_')
                .substring(0, 11);
            return new DummyPrenotazione(ytbase + txt, date);
        }

        @Override
        public void free(IPrenotazione p) {
            //blank
        }
    }
}



