package it.smartteaching.prototype.model;

import it.smartteaching.prototype.model.Model.Identifiable;
import static it.smartteaching.prototype.model.Model.MODEL;
import static it.smartteaching.prototype.model.classiPersone.*;
import java.util.*;
import java.time.*;
import java.util.stream.Collectors;
import static it.smartteaching.prototype.control.Controller.CONTROLLER;

public interface classiChat {
    public static class Messaggio {
        public final Persona mittente;
        public final String contenuto;
        public final LocalDateTime data;

        public Messaggio(Persona mittente, String contenuto, LocalDateTime data) {
            this.mittente = mittente;
            this.contenuto = contenuto;
            this.data = data;
        }
    }


    public static enum TipoChat { PRIVATO, BROADCAST }

    public static abstract class Chat extends Identifiable<String> {
        public final TipoChat tipo;
        public final Set<Persona> partecipanti;

        public Chat(String id, TipoChat tipo, Set<Persona> partecipanti) {
            super(id);
            this.tipo = tipo;
            this.partecipanti = partecipanti;
        }

        public abstract String getLabel();
        public List<Persona> getOthers() {
            Persona p = CONTROLLER.getPersona();
            return this.partecipanti.stream()
                .filter(p2 -> !p2.equals(p))
                .collect(Collectors.toList());
        }
        public List<Messaggio> getMessaggi() {
            return MODEL.getMessaggi(this);
        }
        public abstract void mandaMessaggio(Messaggio msg);
    }
    public static class ChatPrivata extends Chat {
        public ChatPrivata(String id, Set<Persona> partecipanti) {
            super(id, TipoChat.PRIVATO, partecipanti);
        }
        @Override
        public String getLabel() {
            return getOthers().stream()
                .map(p2 -> p2.nome)
                .collect(Collectors.joining(", "));
        }
        @Override
        public void mandaMessaggio(Messaggio msg) {
            MODEL.mandaMessaggio(this, msg, true);
        }
    }
    public static class ChatBroadcast extends Chat {
        public final String label;
        
        public ChatBroadcast(String id, String label, Set<Persona> partecipanti) {
            super(id, TipoChat.BROADCAST, partecipanti);
            this.label = label;
        }
        @Override
        public String getLabel() {
            return this.label;
        }
        @Override
        public void mandaMessaggio(Messaggio msg) {
            MODEL.mandaMessaggio(this, msg, false);
        }
    }
}

