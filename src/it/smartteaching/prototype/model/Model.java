package it.smartteaching.prototype.model;

import static it.smartteaching.prototype.Core.*;
import static it.smartteaching.prototype.model.classiPersone.*;
import static it.smartteaching.prototype.model.classiLezione.*;
import static it.smartteaching.prototype.model.classiFile.*;
import static it.smartteaching.prototype.model.classiChat.*;
import it.smartteaching.prototype.model.classiLaurea.Laurea;
import java.io.InputStream;
import java.util.*;
import java.nio.file.*;
import java.util.function.Consumer;

public interface Model {
    public static abstract class Identifiable<T> {
        public final T id;
        public Identifiable(T id) {
            this.id = id;
        }
        @Override
        public int hashCode() {
            return id.hashCode();
        }
        @Override
        public boolean equals(Object obj) {
            return obj instanceof Identifiable && hashCode() == obj.hashCode();
        }
    }
    public static final Model MODEL = new JSONModel(Path.of("./db"));
    public static enum DataOperation { ADD, UPDATE, DELETE };

    Persona getIdentity(String matricola);
    void aggiornaAccesso(String matricola);

    Persona login(String matricola, String password) throws InvalidCredentialsException;
    Laurea getLaurea(Studente s);
    List<Insegnamento> getInsegnamenti(Laurea l);
    List<Insegnamento> getInsegnamenti(Persona p, boolean coinvolto);
    Set<Laurea> getCorsiLaureaInsegnamento(Insegnamento ins);

    Set<Studente> getIscritti(Insegnamento ins);
    void modificaIscrizione(Studente s, Insegnamento ins, DataOperation op);

    boolean isDocente(Insegnamento ins, Docente d);
    Set<Docente> getDocenti(Insegnamento ins);
    List<Lezione> getLezioni(Insegnamento ins);
    void modificaLezione(Lezione l, DataOperation op, Changes<Materiale> em, Changes<Compito> ea);

    Set<Studente> getPartecipanti(Lezione l);
    void aggiungiPartecipante(Lezione l, Studente s);

    List<Materiale> getMateriali(Lezione l);
    void modificaMateriale(Materiale m, DataOperation op);
    void eliminaMateriali(String id_ins, String id_lez);

    List<Compito> getCompiti(Lezione l);
    void modificaCompito(Compito c, DataOperation op);
    void eliminaCompiti(String id_ins, String id_lez);

    Consegna getConsegna(Compito c, Studente s);
    Map<Studente, Consegna> getConsegneCaricate(Compito c);
    void modificaConsegna(Consegna c, DataOperation op);
    void eliminaConsegne(String id_ins, String id_lez, String id_com);

    List<FileRemoto> getListaFiles(String... ids);
    void leggiContenutoFile(String id, Consumer<InputStream> c);
    void scriviContenutoFile(String id, InputStream s);
    String aggiungiFile(FileLocale f);
    void rimuoviFile(String id);

    List<Chat> getRecenti(Persona p);
    void setRecenti(Persona p, List<Chat> lst);
    Chat apriChat(String id);
    Chat apriChatPrivata(Persona p1, Persona p2);
    void salvaChat(Chat c);
    List<Messaggio> getMessaggi(Chat c);
    void mandaMessaggio(Chat c, Messaggio m, boolean salva_recenti);
}
