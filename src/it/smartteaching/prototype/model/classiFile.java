package it.smartteaching.prototype.model;

import it.smartteaching.prototype.Core.Pair;
import static it.smartteaching.prototype.Core.*;
import it.smartteaching.prototype.model.Model.Identifiable;
import static it.smartteaching.prototype.model.Model.MODEL;
import static it.smartteaching.prototype.model.classiPersone.*;
import static it.smartteaching.prototype.model.classiLezione.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.time.*;
import java.util.function.Consumer;
import java.util.logging.*;

public interface classiFile {
    public static interface IFile {
        public static Pair<String, String> getInfo(IFile... files) {
            String newDim = "#";
            String newTipo = "#";
            if (files.length > 0) {
                if (files.length > 1) {
                    newTipo = "(multipli)";
                    long dimtot = 0;

                    for (IFile sel : files) {
                        if (sel instanceof FileRemoto) {
                            FileRemoto f = (FileRemoto)sel;
                            dimtot += f.dimensione;
                        } else if (sel instanceof FileLocale) {
                            FileLocale f = (FileLocale)sel;
                            if (!f.file.isFile()) {
                                dimtot = -1;
                                break;
                            }
                            dimtot += f.file.length();
                        }
                    }

                    newDim = dimtot < 0? "N/A" : niceBytes(dimtot);

                } else {
                    IFile sel = files[0];
                    if (sel instanceof FileRemoto) {
                        FileRemoto f = (FileRemoto)sel;
                        newTipo = f.mimetype;
                        newDim = niceBytes(f.dimensione);
                    } else if (sel instanceof FileLocale) {
                        FileLocale f = (FileLocale)sel;
                        if (f.file.isFile()) {
                            Path source = Paths.get(f.file.getPath());
                            try {
                                newTipo = Files.probeContentType(source);
                            } catch (IOException ex) {
                                Logger.getLogger(IFile.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            newDim = niceBytes(f.file.length());
                        } else
                            newTipo = "(non un file)";
                    }
                }
            }
            if (newTipo == null)
                newTipo = "(sconosciuto)";

            return new Pair(newDim, newTipo);
        }
    };

    public static class FileLocale implements IFile {
        public final File file;

        public FileLocale(File file) {
            this.file = file;
        }

        @Override
        public String toString() {
            return this.file.getName();
        }
    }

    public static class FileRemoto extends Identifiable<String> implements IFile {
        public final String nome;
        public final String mimetype;
        public final long dimensione;

        public FileRemoto(String id, String nome, String mimetype, long dimensione) {
            super(id);
            this.nome = nome;
            this.mimetype = mimetype;
            this.dimensione = dimensione;
        }

        public void leggiContenuto(Consumer<InputStream> c) {
            MODEL.leggiContenutoFile(this.id, c);
        }
        public void scriviContenuto(InputStream s) {
            MODEL.scriviContenutoFile(this.id, s);
        }
        public boolean scaricaIn(Path directory) {
            Path p = directory.resolve(nome);
            final Boolean[] ret = new Boolean[] { false };
            this.leggiContenuto(remote -> {
                try {
                    Files.copy(remote, p, StandardCopyOption.REPLACE_EXISTING);
                    ret[0] = true;
                } catch (IOException ex) {}
            });
            return ret[0];
        }

        @Override
        public String toString() {
            return this.nome;
        }
    }

    public static class Materiale extends Identifiable<String> {
        public Lezione lezione;
        public List<IFile> contenuti;
        public String nome;

        public Materiale(String id, Lezione lezione, List<IFile> contenuti, String nome) {
            super(id);
            this.lezione = lezione;
            this.contenuti = contenuti;
            this.nome = nome;
        }

        @Override
        public String toString() {
            return this.nome;
        }
    }

    public static class Consegna {
        public final Compito compito;
        public final Studente studente;
        public List<IFile> caricati;
        public LocalDateTime consegna;
        public int valutazione;

        public Consegna(
            Compito compito,
            Studente studente,
            List<IFile> caricati,
            LocalDateTime consegna,
            int valutazione
        ) {
            this.compito = compito;
            this.studente = studente;
            this.caricati = caricati;
            this.consegna = consegna;
            this.valutazione = valutazione;
        }

        @Override
        public String toString() {
            return join(" - ", this.studente.nome, valutazione<0? null : valutazione+"");
        }
    }

    public static class Compito extends Identifiable<String> {
        public Lezione lezione;
        public String nome, descrizione;
        public LocalDateTime ultimaModifica;
        public LocalDateTime scadenza;

        public Compito(
            String id,
            Lezione lezione,
            String nome,
            String descrizione,
            LocalDateTime ultimaModifica,
            LocalDateTime scadenza
        ) {
            super(id);
            this.lezione = lezione;
            this.nome = nome;
            this.descrizione = descrizione;
            this.ultimaModifica = ultimaModifica;
            this.scadenza = scadenza;
        }

        public Consegna getConsegna(Studente s) {
            return MODEL.getConsegna(this, s);
        }
        public Map<Studente, Consegna> getConsegneCaricate() {
            return MODEL.getConsegneCaricate(this);
        }

        @Override
        public String toString() {
            return this.nome;
        }
    }
}
