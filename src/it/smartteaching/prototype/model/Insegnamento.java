package it.smartteaching.prototype.model;

import static it.smartteaching.prototype.Core.*;
import it.smartteaching.prototype.model.Model.Identifiable;
import static it.smartteaching.prototype.model.Model.MODEL;
import static it.smartteaching.prototype.model.classiPersone.*;
import static it.smartteaching.prototype.model.classiLezione.*;
import java.util.*;
import java.util.stream.Collectors;
import static it.smartteaching.prototype.control.Controller.CONTROLLER;

public class Insegnamento extends Identifiable<String> {
    public final String nome;
    public final Docente titolare;
    public final String programma;
    public final int ore_complessive;

    public Insegnamento(
        String id,
        String nome,
        Docente titolare,
        String programma,
        int ore_complessive
    ) {
        super(id);
        this.nome = nome;
        //assertNonEmpty(docenti, "docenti");
        //this.docenti = docenti;
        //if (!docenti.contains(titolare))
        //    throw new IllegalArgumentException("Docente titolare non compreso nell'insieme docenti.");
        //this.iscritti = iscritti;
        this.titolare = titolare;
        this.programma = programma;
        this.ore_complessive = ore_complessive;
    }

    public boolean isResponsabile(Docente d) {
        return MODEL.getCorsiLaureaInsegnamento(this).stream().anyMatch(l -> l.responsabile.equals(d));
    }
    public boolean isDocente(Docente d) {
        return Model.MODEL.isDocente(this, d);
    }
    public List<Lezione> getLezioni() {
        return Model.MODEL.getLezioni(this);
    }
    public Set<Studente> getIscritti() {
        return Model.MODEL.getIscritti(this);
    }
    public Set<Docente> getDocenti() {
        return Model.MODEL.getDocenti(this);
    }
    public int getMinutiRimasti(List<Lezione> lezioni) {
        return (ore_complessive * 60) - lezioni.stream()
            .map(l -> l.durata)
            .reduce(0, Integer::sum);
    }
    public static List<Lezione> quali_lezioni_presenza(List<Lezione> lezioni) {
        return lezioni.stream()
            .filter(l -> l.tipo == TipoLezione.PRESENZA)
            .collect(Collectors.toList());
    }
    public static int quante_lezioni_streaming(List<Lezione> lezioni) {
        return (int)lezioni.stream()
            .filter(l -> l.tipo == TipoLezione.STREAMING)
            .count();
    }
    //int minuti_rimasti() {
    //    int totale = lezioni.stream().collect(Collectors.summingInt(l -> l.durata));
    //    return (ore_complessive * 60) - totale;
    //}
    //Il sistema tiene traccia di QUALI lezioni vengono svolte in presenza
    //e di QUANTE sono svolte in smart working.
    //Set<Lezione> lezioni_presenza() {
    //    return lezioni.stream()
    //        .filter(l -> l.tipo == TipoLezione.PRESENZA)
    //        .collect(Collectors.toSet());
    //}
    //int lezioni_streaming() {
    //    return (int)lezioni.stream()
    //        .filter(l -> l.tipo == TipoLezione.STREAMING)
    //        .count();
    //}

    @Override
    public String toString() {
        return this.nome;
    }
}
