package it.smartteaching.prototype.model;

import static it.smartteaching.prototype.Core.*;
import it.smartteaching.prototype.model.Model.Identifiable;
import static it.smartteaching.prototype.model.classiPersone.*;
import static it.smartteaching.prototype.model.classiFile.*;
import static it.smartteaching.prototype.model.Model.MODEL;
import it.smartteaching.prototype.model.classiConferencePlatform.DummyPrenotazione;
import java.time.*;
import java.util.*;

public interface classiLezione {
    public static enum TipoLezione {
        PRESENZA,
        STREAMING,
        SOLO_MATERIALE
    }

    public static abstract class Lezione extends Identifiable<String> {
        public final TipoLezione tipo;
        public String nome;
        public Insegnamento insegnamento;
        public Docente docente;
        public int durata;
        public LocalDateTime data;

        public Lezione(
            String id,
            String nome,
            TipoLezione tipo,
            Insegnamento insegnamento,
            Docente docente,
            int durata,
            LocalDateTime data
        ) {
            super(id);
            this.nome = nome;
            this.tipo = tipo;
            this.durata = durata;
            this.data = data;
            this.insegnamento = insegnamento;
            this.docente = docente;
        }

        public List<Materiale> getMateriali() {
            return MODEL.getMateriali(this);
        };
        public List<Compito> getCompiti() {
            return MODEL.getCompiti(this);
        };
        public Set<Studente> getPartecipanti() {
            return MODEL.getPartecipanti(this);
        };
    }

    public static class LezionePresenza extends Lezione {
        public LezionePresenza (
            String id,
            String nome,
            Insegnamento insegnamento,
            Docente docente,
            int durata,
            LocalDateTime data
        ) {
            super(
                id,
                nome,
                TipoLezione.PRESENZA,
                insegnamento,
                docente,
                durata, data
            );
        }
    }

    public static class LezioneStreaming extends Lezione {
        public DummyPrenotazione link;

        public LezioneStreaming (
            String id,
            String nome,
            Insegnamento insegnamento,
            Docente docente,
            int durata,
            LocalDateTime data,
            DummyPrenotazione link
        ) {
            super(
                id,
                nome,
                TipoLezione.STREAMING,
                insegnamento,
                docente,
                durata, data
            );
            this.link = link;
        }
        public void aggiungiPartecipante(Studente s) {
            MODEL.aggiungiPartecipante(this, s);
        };
    }

    public static class LezioneSoloMateriale extends Lezione {
        public LezioneSoloMateriale (
            String id,
            String nome,
            Insegnamento insegnamento,
            Docente docente,
            LocalDateTime data
        ) {
            super(
                id,
                nome,
                TipoLezione.SOLO_MATERIALE,
                insegnamento,
                docente,
                0, data
            );
            //assertNonEmpty(materiale, "materiale");
        }
    }
}
