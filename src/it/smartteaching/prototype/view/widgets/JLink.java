package it.smartteaching.prototype.view.widgets;

//import it.smartteaching.prototype.control.Control;
import java.awt.*;
import java.awt.event.*;
import java.util.function.Consumer;
import javax.swing.*;

public class JLink extends JLabel {
    private String actualText;
    private boolean hovering = false;

    public JLink() {
        this.setForeground(Color.blue.darker());
        this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        this.addMouseListener(new MouseAdapter() {
            private final JLink lt = JLink.this;

            @Override
            public void mouseEntered(MouseEvent evt) {
                lt.hovering = true;
                lt.setText(lt.actualText);
            }

            @Override
            public void mouseExited(MouseEvent evt) {
                lt.hovering = false;
                lt.setText(lt.actualText);
            }

            @Override
            public void mousePressed(MouseEvent evt) {
                lt.setForeground(Color.cyan.darker());
            }

            @Override
            public void mouseReleased(MouseEvent evt) {
                lt.setForeground(Color.blue.darker());
            }
        });
    }

    public void addClickHandler(Consumer<MouseEvent> c) {
        this.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                c.accept(evt);
            }
        });
    }

    @Override
    public void setText(String text) {
        actualText = text;
        if (this.isEnabled() && hovering)
            super.setText("<html><font size='14pt'><u>" + actualText + "</u></font></html>");
        else
            super.setText("<html><font size='14pt'>" + actualText + "</font></html>");
    }
}
