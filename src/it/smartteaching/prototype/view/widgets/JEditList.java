package it.smartteaching.prototype.view.widgets;

import it.smartteaching.prototype.Core.IReadOnly;
import it.smartteaching.prototype.view.FormCommon;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.DefaultListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

public class JEditList extends javax.swing.JPanel implements IReadOnly {
    public static interface ISelectionHandler {
        void action();
    }
    public static interface IEditHandler {
        void edit(int index, Object item);
    }

    private boolean readonly = false;
    public final DefaultListModel itemList;
    public final ListSelectionModel itemSelection;
    public ISelectionHandler selectionHandler;
    public IEditHandler editHandler;

    public JEditList() {
        initComponents();
        itemList = new DefaultListModel();
        lstContents.setModel(itemList);
        itemSelection = lstContents.getSelectionModel();
        itemSelection.addListSelectionListener(l -> showSelectionData());
        itemSelection.setSelectionInterval(0, 0);
        itemList.addListDataListener(new ListDataListener() {
            @Override
            public void intervalAdded(ListDataEvent lde) {
                JEditList.this.updateRemButton();
            }

            @Override
            public void intervalRemoved(ListDataEvent lde) {
                JEditList.this.updateRemButton();
            }

            @Override
            public void contentsChanged(ListDataEvent lde) {
                JEditList.this.updateRemButton();
            }
        });

        itemList.clear();
        jAddRem.btnRem.setEnabled(false);
        jAddRem.btnRem.addActionListener(l -> removeSelected());
    }

    public <T> void loadList(List<T> objs) {
        itemList.clear();
        for (T o : objs)
            itemList.addElement(o);
        if (objs.size() > 0)
            lstContents.setSelectedIndex(0);
    }

    public <T> List<T> getItemsObjects() {
        return Arrays.stream(itemList.toArray())
            .map(o -> (T)o)
            .collect(Collectors.toList());
    }

    public void showSelectionData() {
        if (selectionHandler != null)
            selectionHandler.action();
        revalidate();
        repaint();
    }

    private void editSelected() {
        if (this.readonly || editHandler == null)
            return;
        int[] in = itemSelection.getSelectedIndices();
        if (in.length > 0)
            editHandler.edit(in[0], itemList.getElementAt(in[0]));
        showSelectionData();
    }

    private void removeSelected() {
        if (this.readonly || itemList.size() <= 0)
            return;
        int[] in = itemSelection.getSelectedIndices();
        Object[] objs = Arrays.stream(in)
            .filter(i -> i >= 0 && i < itemList.size())
            .mapToObj(i -> itemList.getElementAt(i))
            .toArray();
        Arrays.asList(objs).forEach(o -> itemList.removeElement(o));
        itemSelection.setSelectionInterval(
            Integer.max(0, in[0]),
            Integer.min(in[0], itemList.size() - 1)
        );
    }

    private void updateRemButton() {
        jAddRem.btnRem.setEnabled(itemList.size() > 0);
    }

    public void setReadOnly(boolean option) {
        this.readonly = option;
        jAddRem.setEnabled(!option);
        jAddRem.setVisible(!option);
    }
    @Override
    public boolean isReadOnly() {
        return this.readonly;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        lstContents = new javax.swing.JList<>();
        jAddRem = new it.smartteaching.prototype.view.widgets.JAddRem();
        lblType = new javax.swing.JLabel();

        jScrollPane2.setPreferredSize(new java.awt.Dimension(0, 0));

        lstContents.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, FormCommon.BORDER_COLOR));
        lstContents.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "item1" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        lstContents.setPreferredSize(new java.awt.Dimension(0, 0));
        lstContents.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstContentsMouseClicked(evt);
            }
        });
        lstContents.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                lstContentsKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(lstContents);

        lblType.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        lblType.setText("List:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblType)
                    .addComponent(jAddRem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lblType)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jAddRem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void lstContentsKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lstContentsKeyPressed
        switch (evt.getKeyCode()) {
            case KeyEvent.VK_DELETE:
                removeSelected();
                break;
            case KeyEvent.VK_ENTER:
                editSelected();
                break;
        }
    }//GEN-LAST:event_lstContentsKeyPressed

    private void lstContentsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstContentsMouseClicked
        if (evt.getClickCount() == 2)
            editSelected();
    }//GEN-LAST:event_lstContentsMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public it.smartteaching.prototype.view.widgets.JAddRem jAddRem;
    public javax.swing.JScrollPane jScrollPane2;
    public javax.swing.JLabel lblType;
    public javax.swing.JList<String> lstContents;
    // End of variables declaration//GEN-END:variables
}
