package it.smartteaching.prototype.view.widgets;

import static it.smartteaching.prototype.Core.*;
import it.smartteaching.prototype.model.*;
import it.smartteaching.prototype.view.FormCommon;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.util.function.*;
import static it.smartteaching.prototype.control.Controller.CONTROLLER;

public class JViewInsegnamento extends javax.swing.JPanel {
    public final Insegnamento ins;
    public final boolean coinvolto, responsabile;

    public JViewInsegnamento(Insegnamento ins, boolean coinvolto, boolean responsabile) {
        initComponents();
        this.ins = ins;
        this.coinvolto = coinvolto;
        this.responsabile = responsabile;
        updateText();
    }
    private void updateText() {
        lnkNome.setText(ins.nome);
        setSameFontSize(lnkNome, 16);
        lblTitolare.setText("- " + ins.titolare.nome);
        txtDesc.setText(ins.programma);
        txtDesc.setMaximumSize(txtDesc.getPreferredSize());
        txtDesc.setCaretPosition(0);
        lnkNome.setEnabled(coinvolto || responsabile);
        lnkToggleSubscrib.setEnabled(!CONTROLLER.isDocente());
        lnkToggleSubscrib.setVisible(!CONTROLLER.isDocente());
        lnkToggleSubscrib.setText(coinvolto? "lascia" : "iscriviti");
        this.setMaximumSize(new Dimension(Short.MAX_VALUE, this.getPreferredSize().height));
    }
    public void addShowEventHandler(BiConsumer<JViewInsegnamento, MouseEvent> handler) {
        lnkNome.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (lnkNome.isEnabled())
                    handler.accept(JViewInsegnamento.this, evt);
            }
        });
    }
    public void addToggleSubEventHandler(BiConsumer<JViewInsegnamento, MouseEvent> handler) {
        lnkToggleSubscrib.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                handler.accept(JViewInsegnamento.this, evt);
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lnkNome = new it.smartteaching.prototype.view.widgets.JLink();
        lblTitolare = new javax.swing.JLabel();
        lnkToggleSubscrib = new it.smartteaching.prototype.view.widgets.JLink();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtDesc = new javax.swing.JTextArea();

        jPanel1.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, FormCommon.BORDER_COLOR));

        lnkNome.setText("NOME");

        lblTitolare.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        lblTitolare.setText("- TITOLARE");
        lblTitolare.setToolTipText("");

        lnkToggleSubscrib.setText("iscriviti");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(lnkNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTitolare)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lnkToggleSubscrib, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lnkNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTitolare)
                    .addComponent(lnkToggleSubscrib, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jScrollPane2.setBorder(null);
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane2.setViewportBorder(null);
        jScrollPane2.setAutoscrolls(true);
        jScrollPane2.setOpaque(false);
        jScrollPane2.setWheelScrollingEnabled(false);

        txtDesc.setEditable(false);
        txtDesc.setLineWrap(true);
        txtDesc.setTabSize(4);
        txtDesc.setText("DESCRIZIONE");
        txtDesc.setWrapStyleWord(true);
        txtDesc.setBorder(null);
        txtDesc.setOpaque(false);
        jScrollPane2.setViewportView(txtDesc);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jScrollPane2)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblTitolare;
    private it.smartteaching.prototype.view.widgets.JLink lnkNome;
    private it.smartteaching.prototype.view.widgets.JLink lnkToggleSubscrib;
    private javax.swing.JTextArea txtDesc;
    // End of variables declaration//GEN-END:variables
}
