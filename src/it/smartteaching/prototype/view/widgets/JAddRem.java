package it.smartteaching.prototype.view.widgets;

public class JAddRem extends javax.swing.JPanel {
    public JAddRem() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAdd = new javax.swing.JButton();
        btnRem = new javax.swing.JButton();

        btnAdd.setText("+");
        btnAdd.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAdd.setIconTextGap(0);
        btnAdd.setMaximumSize(new java.awt.Dimension(35, 35));
        btnAdd.setMinimumSize(new java.awt.Dimension(35, 35));
        btnAdd.setPreferredSize(new java.awt.Dimension(35, 35));

        btnRem.setText("-");
        btnRem.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRem.setIconTextGap(0);
        btnRem.setMaximumSize(new java.awt.Dimension(35, 35));
        btnRem.setMinimumSize(new java.awt.Dimension(35, 35));
        btnRem.setPreferredSize(new java.awt.Dimension(35, 35));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnAdd, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnRem, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnRem, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnAdd;
    public javax.swing.JButton btnRem;
    // End of variables declaration//GEN-END:variables
}
