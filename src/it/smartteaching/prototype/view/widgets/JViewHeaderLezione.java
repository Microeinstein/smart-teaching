package it.smartteaching.prototype.view.widgets;

import static it.smartteaching.prototype.Core.CustomDateTimeFormat;
import it.smartteaching.prototype.model.classiLezione;
import it.smartteaching.prototype.model.classiLezione.Lezione;
import it.smartteaching.prototype.view.FormCommon;
import java.util.List;

public class JViewHeaderLezione extends javax.swing.JPanel {
    public JViewHeaderLezione() {
        initComponents();
    }
    public void showData(Lezione l) {
        txtNome.setText(l.nome);
        txtNome.setToolTipText(l.docente.nome);
        txtData.setText(l.data.format(CustomDateTimeFormat));
        int min = l.durata;
        int hour = min / 60;
        min -= hour * 60;
        String durata = "";
        if (!(l instanceof classiLezione.LezioneSoloMateriale)) {
            durata = "(";
            durata += String.join(" ", List.of(
                    (hour>0 ? hour + "h" : ""),
                    (min>=0 ? min + "m" : "")
                ).stream().filter(s -> !s.isEmpty()).toArray(String[]::new)
            );
            durata += ")";
        }
        txtDurata.setText(durata);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtNome = new javax.swing.JLabel();
        txtDurata = new javax.swing.JLabel();
        txtData = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, FormCommon.BORDER_COLOR));
        setMaximumSize(new java.awt.Dimension(32767, 22));
        setPreferredSize(new java.awt.Dimension(50, 22));

        txtNome.setFont(new java.awt.Font("Inter", 1, 13)); // NOI18N
        txtNome.setText("NOME");

        txtDurata.setText("DURATA");

        txtData.setText("DATA");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(txtNome)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtDurata)
                .addGap(18, 18, 18)
                .addComponent(txtData)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDurata)
                    .addComponent(txtData)
                    .addComponent(txtNome))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel txtData;
    private javax.swing.JLabel txtDurata;
    private javax.swing.JLabel txtNome;
    // End of variables declaration//GEN-END:variables
}
