package it.smartteaching.prototype.view.widgets;

import static it.smartteaching.prototype.Core.*;
import static it.smartteaching.prototype.model.classiFile.*;
//import java.awt.FileDialog;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.TransferHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.FileDialog;
import static it.smartteaching.prototype.control.Controller.CONTROLLER;

public class JEditFiles extends javax.swing.JPanel implements IReadOnly {
    private static class FileDropHandler extends TransferHandler {
        public static interface IFileHandler {
            public void action(File f);
        }

        private final IReadOnly control;
        private final IFileHandler handler;
        public FileDropHandler(IReadOnly control, IFileHandler h) {
            this.control = control;
            this.handler = h;
        }

        @Override
        public boolean canImport(TransferHandler.TransferSupport support) {
            if (this.control.isReadOnly())
                return false;
            for (DataFlavor flavor : support.getDataFlavors()) {
                if (flavor.isFlavorJavaFileListType())
                    return true;
            }
            return false;
        }

        @Override
        @SuppressWarnings("unchecked")
        public boolean importData(TransferHandler.TransferSupport support) {
            if (!this.canImport(support))
                return false;

            List<File> files;
            try {
                files = (List<File>)support.getTransferable()
                        .getTransferData(DataFlavor.javaFileListFlavor);
            } catch (UnsupportedFlavorException | IOException ex) {
                // should never happen (or JDK is buggy)
                return false;
            }

            for (File file: files)
                handler.action(file);
            return true;
        }
    }

    private final FileDialog fileDialog;
    private boolean hiddenInfo = false;

    public JEditFiles() {
        initComponents();
        editor.lblType.setText("Files:");
        editor.lstContents.setTransferHandler(new FileDropHandler(editor, this::addFile));
//        fileDialog = new FileDialog((java.awt.Frame)null);
//        fileDialog.setMode(FileDialog.LOAD);
//        fileDialog.setMultipleMode(true);
//        fileDialog.setTitle("Aggiungi files");
//        editor.jAddRem.btnAdd.addActionListener(l -> {
//            fileDialog.setVisible(true);
//            for (File f : fileDialog.getFiles())
//                JEditFiles.this.addFile(f);
//        });
        if (CONTROLLER != null && CONTROLLER.getForm() != null) {
            fileDialog = new FileDialog(CONTROLLER.getForm().shell, SWT.OPEN | SWT.MULTI);
            fileDialog.setText("Carica files");
        } else
            fileDialog = null;
        //String[] filterExt = { "*.txt", "*.doc", ".rtf", "*.*" };
        //fileDialog.setFilterExtensions(filterExt);
        editor.jAddRem.btnAdd.addActionListener(l -> {
            fileDialog.open();
            String rel = fileDialog.getFilterPath();
            for (String s : fileDialog.getFileNames())
                JEditFiles.this.addFile(Path.of(rel, s).toFile());
        });

        editor.selectionHandler = this::showSelectionData;
    }

    public void loadFiles(List<IFile> files) {
        editor.loadList(files);
    }

    void addFile(File f) {
        if (editor.isReadOnly())
            return;
        editor.itemList.addElement(new FileLocale(f));
    }

    public static boolean showFilesInfo(JEditList editor, JLabel lblSize, JLabel lblMIME) {
        int[] in = editor.itemSelection.getSelectedIndices();
        IFile[] sel = Arrays.stream(in)
            .filter(i -> i >= 0 && i < editor.itemList.size())
            .mapToObj(i -> editor.itemList.get(i))
            .toArray(IFile[]::new);
        boolean empty = sel.length < 1;
        Pair<String, String> info = IFile.getInfo(sel);
        lblSize.setText(info.fst);
        lblMIME.setText(info.snd);
        return !empty;
    }
    public void showSelectionData() {
        if (this.hiddenInfo)
            return;
        showFilesInfo(editor, lblSize, lblMIME);
    }

    public List<IFile> getFiles() {
        return editor.getItemsObjects();
    }

    public void setReadOnly(boolean option) {
        editor.setReadOnly(option);
    }
    @Override
    public boolean isReadOnly() {
        return editor.isReadOnly();
    }

    public void setInfoHidden(boolean option) {
        this.hiddenInfo = option;
        showSelectionData();
        this.jLabel2.setVisible(!option);
        this.jLabel4.setVisible(!option);
        this.lblMIME.setVisible(!option);
        this.lblSize.setVisible(!option);
    }
    public boolean isInfoHidden() {
        return this.hiddenInfo;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        lblSize = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblMIME = new javax.swing.JLabel();
        editor = new it.smartteaching.prototype.view.widgets.JEditList();

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel2.setText("Dimensione:");

        lblSize.setFont(new java.awt.Font("JetBrains Mono", 0, 13)); // NOI18N
        lblSize.setText("0B");

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel4.setText("Tipo:");

        lblMIME.setFont(new java.awt.Font("JetBrains Mono", 0, 13)); // NOI18N
        lblMIME.setText("text/plain");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMIME, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
                    .addComponent(lblSize, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addComponent(editor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(editor, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lblSize))
                .addGap(3, 3, 3)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(lblMIME)))
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public it.smartteaching.prototype.view.widgets.JEditList editor;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel lblMIME;
    private javax.swing.JLabel lblSize;
    // End of variables declaration//GEN-END:variables
}
