package it.smartteaching.prototype.view.widgets;

import static it.smartteaching.prototype.Core.*;
import it.smartteaching.prototype.model.classiConferencePlatform.IPrenotazione;
import it.smartteaching.prototype.model.classiFile.*;
import static it.smartteaching.prototype.model.classiLezione.*;
import it.smartteaching.prototype.model.classiPersone.*;
import java.util.*;
import javax.swing.JLabel;
import static it.smartteaching.prototype.control.Controller.CONTROLLER;

public class JViewLezione extends javax.swing.JPanel {
    public final Lezione lez;
    public final boolean doc, insdoc, resp;

    public JViewLezione(Lezione lez, boolean doc, boolean insdoc, boolean resp) {
        initComponents();
        this.lez = lez;
        this.doc = doc;
        this.insdoc = insdoc;
        this.resp = resp;
        updateContent();
    }
    private void updateContent() {
        this.header.showData(lez);

        if (!(lez instanceof LezioneStreaming))
            removeComp(lnkStreaming);
        else {
            IPrenotazione p = ((LezioneStreaming)lez).link;
            lnkStreaming.setToolTipText(p!=null? p.getLink(): null);
        }
        if (!insdoc || !lez.docente.equals(CONTROLLER.getPersona()))
            removeComp(lnkEdit);

        pnlMateriali2.removeAll();
        List<Materiale> lstM = lez.getMateriali();
        if (lstM.isEmpty())
            removeComp(pnlMateriali);
        else {
            for (Materiale m : lstM) {
                JLink lnk = new JLink();
                lnk.setText(m.nome);
                lnk.setMaximumSize(lnk.getPreferredSize());
                lnk.addClickHandler(evt -> CONTROLLER.openMateriale(m));
                pnlMateriali2.add(lnk);
            }
        }

        pnlCompiti2.removeAll();
        List<Compito> lstC = lez.getCompiti();
        if ((doc && !insdoc) || lstC.isEmpty())
            removeComp(pnlCompiti);
        else {
            for (Compito c : lstC) {
                JLink lnk = new JLink();
                lnk.setText(c.nome);
                lnk.setMaximumSize(lnk.getPreferredSize());
                lnk.addClickHandler(evt -> CONTROLLER.openCompito(c));
                pnlCompiti2.add(lnk);
            }
        }
        if (pnlHMC.getComponentCount() == 0)
            removeComp(pnlHMC);

        pnlPartecipanti2.removeAll();
        if (!doc || lez instanceof LezioneSoloMateriale) {
            removeComp(pnlPartecipanti);
        } else {
            Set<Studente> lstP = lez.getPartecipanti();
            if (lstP.isEmpty())
                pnlPartecipanti2.add(new JLabel("(nessuno)"));
            else {
                for (Studente s : lstP) {
                    JLink lnk = new JLink();
                    lnk.addClickHandler(evt -> CONTROLLER.openChat(s));
                    lnk.setText(s.nome);
                    lnk.setToolTipText(s.getLastVisitMsg());
                    lnk.setMaximumSize(lnk.getPreferredSize());
                    pnlPartecipanti2.add(lnk);
                }
            }
        }
        if (pnlVMP.getComponentCount() == 0)
            removeComp(pnlVMP);

        this.validate();
        revalidate();
        repaint();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lnkEdit = new it.smartteaching.prototype.view.widgets.JLink();
        lnkStreaming = new it.smartteaching.prototype.view.widgets.JLink();
        pnlVMP = new javax.swing.JPanel();
        pnlHMC = new javax.swing.JPanel();
        pnlMateriali = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        pnlMateriali2 = new javax.swing.JPanel();
        pnlCompiti = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        pnlCompiti2 = new javax.swing.JPanel();
        pnlPartecipanti = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        pnlPartecipanti2 = new javax.swing.JPanel();
        header = new it.smartteaching.prototype.view.widgets.JViewHeaderLezione();

        setBorder(null);

        lnkEdit.setText("modifica");
        lnkEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkEditMouseClicked(evt);
            }
        });

        lnkStreaming.setText("vai alla lezione in streaming");
        lnkStreaming.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkStreamingMouseClicked(evt);
            }
        });

        pnlVMP.setPreferredSize(new java.awt.Dimension(0, 0));
        pnlVMP.setLayout(new javax.swing.BoxLayout(pnlVMP, javax.swing.BoxLayout.Y_AXIS));

        pnlHMC.setPreferredSize(new java.awt.Dimension(0, 0));
        pnlHMC.setLayout(new javax.swing.BoxLayout(pnlHMC, javax.swing.BoxLayout.LINE_AXIS));

        pnlMateriali.setPreferredSize(new java.awt.Dimension(0, 0));

        jLabel1.setText("Materiali:");

        pnlMateriali2.setLayout(new javax.swing.BoxLayout(pnlMateriali2, javax.swing.BoxLayout.Y_AXIS));

        javax.swing.GroupLayout pnlMaterialiLayout = new javax.swing.GroupLayout(pnlMateriali);
        pnlMateriali.setLayout(pnlMaterialiLayout);
        pnlMaterialiLayout.setHorizontalGroup(
            pnlMaterialiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMaterialiLayout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(pnlMaterialiLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlMateriali2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlMaterialiLayout.setVerticalGroup(
            pnlMaterialiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMaterialiLayout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(0, 0, 0)
                .addComponent(pnlMateriali2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlHMC.add(pnlMateriali);

        pnlCompiti.setPreferredSize(new java.awt.Dimension(0, 0));

        jLabel2.setText("Compiti:");

        pnlCompiti2.setLayout(new javax.swing.BoxLayout(pnlCompiti2, javax.swing.BoxLayout.Y_AXIS));

        javax.swing.GroupLayout pnlCompitiLayout = new javax.swing.GroupLayout(pnlCompiti);
        pnlCompiti.setLayout(pnlCompitiLayout);
        pnlCompitiLayout.setHorizontalGroup(
            pnlCompitiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCompitiLayout.createSequentialGroup()
                .addComponent(jLabel2)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(pnlCompitiLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlCompiti2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlCompitiLayout.setVerticalGroup(
            pnlCompitiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCompitiLayout.createSequentialGroup()
                .addComponent(jLabel2)
                .addGap(0, 0, 0)
                .addComponent(pnlCompiti2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlHMC.add(pnlCompiti);

        pnlVMP.add(pnlHMC);

        pnlPartecipanti.setPreferredSize(new java.awt.Dimension(0, 0));

        jLabel3.setText("Partecipanti:");

        pnlPartecipanti2.setPreferredSize(new java.awt.Dimension(0, 0));
        pnlPartecipanti2.setLayout(new javax.swing.BoxLayout(pnlPartecipanti2, javax.swing.BoxLayout.Y_AXIS));

        javax.swing.GroupLayout pnlPartecipantiLayout = new javax.swing.GroupLayout(pnlPartecipanti);
        pnlPartecipanti.setLayout(pnlPartecipantiLayout);
        pnlPartecipantiLayout.setHorizontalGroup(
            pnlPartecipantiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPartecipantiLayout.createSequentialGroup()
                .addComponent(jLabel3)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(pnlPartecipantiLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlPartecipanti2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlPartecipantiLayout.setVerticalGroup(
            pnlPartecipantiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPartecipantiLayout.createSequentialGroup()
                .addComponent(jLabel3)
                .addGap(0, 0, 0)
                .addComponent(pnlPartecipanti2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlVMP.add(pnlPartecipanti);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(header, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlVMP, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lnkEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lnkStreaming, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(header, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lnkEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lnkStreaming, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlVMP, javax.swing.GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void lnkEditMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lnkEditMouseClicked
        CONTROLLER.editLesson(lez, false);
    }//GEN-LAST:event_lnkEditMouseClicked

    private void lnkStreamingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lnkStreamingMouseClicked
        LezioneStreaming l = (LezioneStreaming)this.lez;
        if (!doc)
            l.aggiungiPartecipante((Studente)CONTROLLER.getPersona());
        openLink(l.link.getLink());
    }//GEN-LAST:event_lnkStreamingMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private it.smartteaching.prototype.view.widgets.JViewHeaderLezione header;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private it.smartteaching.prototype.view.widgets.JLink lnkEdit;
    private it.smartteaching.prototype.view.widgets.JLink lnkStreaming;
    private javax.swing.JPanel pnlCompiti;
    private javax.swing.JPanel pnlCompiti2;
    private javax.swing.JPanel pnlHMC;
    private javax.swing.JPanel pnlMateriali;
    private javax.swing.JPanel pnlMateriali2;
    private javax.swing.JPanel pnlPartecipanti;
    private javax.swing.JPanel pnlPartecipanti2;
    private javax.swing.JPanel pnlVMP;
    // End of variables declaration//GEN-END:variables
}
