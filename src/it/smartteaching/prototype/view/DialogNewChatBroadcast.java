package it.smartteaching.prototype.view;

import it.smartteaching.prototype.Core.IDataUserEditor;
import static it.smartteaching.prototype.Core.genid;
import it.smartteaching.prototype.model.Insegnamento;
import it.smartteaching.prototype.model.Model;
import static it.smartteaching.prototype.model.Model.MODEL;
import it.smartteaching.prototype.model.classiChat.ChatBroadcast;
import it.smartteaching.prototype.model.classiPersone.Persona;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.swing.DefaultListModel;
import javax.swing.ListSelectionModel;
import static it.smartteaching.prototype.control.Controller.CONTROLLER;

public class DialogNewChatBroadcast extends javax.swing.JDialog implements IDataUserEditor<ChatBroadcast> {
    private boolean ok = false;
    public final DefaultListModel itemList1, itemList2;
    public final ListSelectionModel itemSelection1, itemSelection2;
    
    public DialogNewChatBroadcast() {
        super(CONTROLLER.getForm(), ModalityType.APPLICATION_MODAL);
        initComponents();
        itemList1 = new DefaultListModel();
        lstPersone.setModel(itemList1);
        itemSelection1 = lstPersone.getSelectionModel();
        itemList2 = new DefaultListModel();
        lstInsegnamenti.setModel(itemList2);
        itemSelection2 = lstInsegnamenti.getSelectionModel();
        loadEntries();
        this.setLocationRelativeTo(CONTROLLER.getForm());
    }
    private void loadEntries() {
        Persona p = CONTROLLER.getPersona();
        Set<Persona> pers = new TreeSet<>((a, b) -> a.compareTo(b));
        Set<Insegnamento> inss = new TreeSet<>((a, b) -> a.nome.compareTo(b.nome));
        for (Insegnamento ins : MODEL.getInsegnamenti(p, true)) {
            ins.getIscritti().forEach(p2 -> pers.add(p2));
            ins.getDocenti().forEach(p2 -> pers.add(p2));
            inss.add(ins);
        }
        itemList1.addAll(pers);
        itemList2.addAll(inss);
    }
    
    @Override
    public boolean isApproved() {
        return ok;
    }
    @Override
    public Model.DataOperation getChoice() {
        return Model.DataOperation.ADD;
    }
    @Override
    public ChatBroadcast applyData() {
        int[] in1 = itemSelection1.getSelectedIndices();
        int[] in2 = itemSelection2.getSelectedIndices();
        boolean isSel1 = in1.length > 0;
        boolean isSel2 = in2.length > 0;
        if (!isSel1 && !isSel2)
            return null;
        Stream<Persona> sel1 = Arrays.stream(in1)
            .filter(i -> i >= 0 && i < itemList1.size())
            .mapToObj(i -> (Persona)itemList1.get(i));
        Stream<Persona> sel2 = Arrays.stream(in2)
            .filter(i -> i >= 0 && i < itemList2.size())
            .mapToObj(i -> ((Insegnamento)itemList2.get(i)).getIscritti())
            .flatMap(s -> s.stream()).map(s -> (Persona)s);
        Set<Persona> sel = Stream.concat(sel1, sel2)
            .sorted().distinct()
            .collect(Collectors.toSet());
        if (sel.size() < 1)
            return null;
        ChatBroadcast cb = new ChatBroadcast(genid(), txtNome.getText(), sel);
        return cb;
    }
    @Override
    public void undoPending() {}

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstPersone = new javax.swing.JList<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstInsegnamenti = new javax.swing.JList<>();
        btnOk = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        btnCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Nuova chat di broadcast");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel3.setLayout(new java.awt.GridLayout(1, 2, 5, 0));

        jScrollPane1.setViewportView(lstPersone);

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel3.setText("Persone");

        jLabel1.setText("Destinatari:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 222, Short.MAX_VALUE))
        );

        jPanel3.add(jPanel1);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel2.setText("Insegnamenti");

        jScrollPane2.setViewportView(lstInsegnamenti);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 222, Short.MAX_VALUE))
        );

        jPanel3.add(jPanel2);

        btnOk.setText("Apri");
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        jLabel4.setText("Nome chat:");

        txtNome.setText("broadcast");

        btnCancel.setText("Annulla");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(6, 6, 6)
                        .addComponent(txtNome))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        ok = true;
        this.setVisible(false);
    }//GEN-LAST:event_btnOkActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        ok = false;
        this.setVisible(false);
    }//GEN-LAST:event_btnCancelActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        ok = false;
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnOk;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList<String> lstInsegnamenti;
    private javax.swing.JList<String> lstPersone;
    private javax.swing.JTextField txtNome;
    // End of variables declaration//GEN-END:variables
}
