package it.smartteaching.prototype.view;

import it.smartteaching.prototype.view.widgets.JLink;
import it.smartteaching.prototype.view.widgets.JViewLezione;
import static it.smartteaching.prototype.Core.*;
import static it.smartteaching.prototype.model.Model.MODEL;
import static it.smartteaching.prototype.model.classiLezione.*;
import static it.smartteaching.prototype.model.classiPersone.*;
import it.smartteaching.prototype.model.*;
import java.awt.Component;
import java.awt.Dimension;
import java.time.LocalDateTime;
import javax.swing.Box.Filler;
import java.util.*;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Group;
import static it.smartteaching.prototype.control.Controller.CONTROLLER;

public class PanelInsegnamento extends PanelCommon {
    private Insegnamento ins;

    public PanelInsegnamento(Insegnamento ins) {
        super(ins.nome, ins.nome);
        this.ins = ins;
        initComponents();
        loadData();
    }
    private void loadData() {
        boolean doc = CONTROLLER.isDocente();
        boolean insdoc = doc && ins.isDocente((Docente)CONTROLLER.getPersona());
        boolean resp = doc && ins.isResponsabile((Docente)CONTROLLER.getPersona());
        List<Lezione> lezioni = ins.getLezioni();
        Set<Docente> docenti = ins.getDocenti();
        numStreaming.setText(Insegnamento.quante_lezioni_streaming(lezioni) + "");
        numOreTot.setText(ins.ore_complessive + "");
        timeSet(ins.getMinutiRimasti(lezioni), (h,m) -> {
            numRemaining.setText(String.format("%02d:%02d", h, m));
        });
        lblTitolare.setText("titolare: " + ins.titolare.nome);
        lnkAddLezione.setEnabled(insdoc);
        lnkAddLezione.setVisible(insdoc);
        txtProgramma2.setText(ins.programma);
        txtProgramma2.setSize(txtProgramma2.getPreferredSize());
        pnlIscritti2.removeAll();
        if (doc) {
            ins.getIscritti().forEach(i -> {
                JLink lnk = new JLink();
                lnk.addClickHandler(evt -> CONTROLLER.openChat(i));
                lnk.setText(i.nome);
                lnk.setToolTipText(i.getLastVisitMsg());
                lnk.setMaximumSize(lnk.getPreferredSize());
                pnlIscritti2.add(lnk);
            });
        } else
            removeComp(pnlIscritti);
        pnlDocenti2.removeAll();
        docenti.forEach(d -> {
            //System.err.println(d.nome);
            JLink lnk = new JLink();
            lnk.addClickHandler(evt -> CONTROLLER.openChat(d));
            lnk.setText(d.nome);
            lnk.setToolTipText(d.getLastVisitMsg());
            lnk.setMaximumSize(lnk.getPreferredSize());
            pnlDocenti2.add(lnk);
        });
        lstLezioni.removeAll();
        GroupLayout lgl = (GroupLayout)lstLezioni.getLayout();
        Group gv = lgl.createSequentialGroup();
        Group gh = lgl.createParallelGroup();
        lezioni.forEach(l -> {
            //System.err.println(l.nome);
            JViewLezione v = new JViewLezione(l, doc, insdoc, resp);
            //lstLezioni.add(v);
            gv.addComponent(v,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.DEFAULT_SIZE,
                GroupLayout.PREFERRED_SIZE
            );
            gv.addGap(16);
            gh.addComponent(v);
        });
        lgl.setHorizontalGroup(gh);
        lgl.setVerticalGroup(gv);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        lblTitolare = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstLezioni = new javax.swing.JPanel();
        pnlMenu = new javax.swing.JPanel();
        lnkBack = new it.smartteaching.prototype.view.widgets.JLink();
        lnkAddLezione = new it.smartteaching.prototype.view.widgets.JLink();
        chkSoloPresenza = new javax.swing.JCheckBox();
        pnlTimes = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        numStreaming = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        numOreTot = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        numRemaining = new javax.swing.JLabel();
        pnlIscritti = new javax.swing.JPanel();
        lblIscritti = new javax.swing.JLabel();
        pnlIscritti2 = new javax.swing.JPanel();
        pnlDocenti = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        pnlDocenti2 = new javax.swing.JPanel();
        pnlProgramma = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtProgramma2 = new javax.swing.JTextArea();

        jLabel1.setText("$HEADER");

        lblTitolare.setText("TITOLARE");

        jScrollPane2.setBorder(null);
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane2.setViewportBorder(null);
        jScrollPane2.setOpaque(false);

        javax.swing.GroupLayout lstLezioniLayout = new javax.swing.GroupLayout(lstLezioni);
        lstLezioni.setLayout(lstLezioniLayout);
        lstLezioniLayout.setHorizontalGroup(
            lstLezioniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 465, Short.MAX_VALUE)
        );
        lstLezioniLayout.setVerticalGroup(
            lstLezioniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 450, Short.MAX_VALUE)
        );

        jScrollPane2.setViewportView(lstLezioni);

        pnlMenu.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 0, 1, FormCommon.BORDER_COLOR));
        pnlMenu.setMinimumSize(new java.awt.Dimension(210, 210));
        pnlMenu.setPreferredSize(new java.awt.Dimension(210, 210));

        lnkBack.setText("Indietro");
        lnkBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkBackMouseClicked(evt);
            }
        });

        lnkAddLezione.setText("Aggiungi lezione");
        lnkAddLezione.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkAddLezioneMouseClicked(evt);
            }
        });

        chkSoloPresenza.setText("<html>Mostra solo lezioni<br>in presenza</html>");
        chkSoloPresenza.setPreferredSize(new java.awt.Dimension(195, 36));
        chkSoloPresenza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkSoloPresenzaActionPerformed(evt);
            }
        });

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel3.setText("Lezioni in streaming:");

        numStreaming.setText("0");

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel4.setText("Ore complessive:");

        numOreTot.setText("0");

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel5.setText("Tempo rimanente:");

        numRemaining.setText("0");

        javax.swing.GroupLayout pnlTimesLayout = new javax.swing.GroupLayout(pnlTimes);
        pnlTimes.setLayout(pnlTimesLayout);
        pnlTimesLayout.setHorizontalGroup(
            pnlTimesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTimesLayout.createSequentialGroup()
                .addGroup(pnlTimesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlTimesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(numStreaming, javax.swing.GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
                    .addComponent(numOreTot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(numRemaining, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        pnlTimesLayout.setVerticalGroup(
            pnlTimesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTimesLayout.createSequentialGroup()
                .addGroup(pnlTimesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(numStreaming))
                .addGap(2, 2, 2)
                .addGroup(pnlTimesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(numOreTot))
                .addGap(2, 2, 2)
                .addGroup(pnlTimesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(numRemaining)))
        );

        lblIscritti.setText("Iscritti:");

        pnlIscritti2.setOpaque(false);
        pnlIscritti2.setLayout(new javax.swing.BoxLayout(pnlIscritti2, javax.swing.BoxLayout.Y_AXIS));

        javax.swing.GroupLayout pnlIscrittiLayout = new javax.swing.GroupLayout(pnlIscritti);
        pnlIscritti.setLayout(pnlIscrittiLayout);
        pnlIscrittiLayout.setHorizontalGroup(
            pnlIscrittiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlIscrittiLayout.createSequentialGroup()
                .addGroup(pnlIscrittiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblIscritti)
                    .addGroup(pnlIscrittiLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(pnlIscritti2, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        pnlIscrittiLayout.setVerticalGroup(
            pnlIscrittiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlIscrittiLayout.createSequentialGroup()
                .addComponent(lblIscritti)
                .addGap(0, 0, 0)
                .addComponent(pnlIscritti2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jLabel2.setText("Docenti:");

        pnlDocenti2.setOpaque(false);
        pnlDocenti2.setLayout(new javax.swing.BoxLayout(pnlDocenti2, javax.swing.BoxLayout.Y_AXIS));

        javax.swing.GroupLayout pnlDocentiLayout = new javax.swing.GroupLayout(pnlDocenti);
        pnlDocenti.setLayout(pnlDocentiLayout);
        pnlDocentiLayout.setHorizontalGroup(
            pnlDocentiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDocentiLayout.createSequentialGroup()
                .addGroup(pnlDocentiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(pnlDocentiLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(pnlDocenti2, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(23, 23, 23))
        );
        pnlDocentiLayout.setVerticalGroup(
            pnlDocentiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDocentiLayout.createSequentialGroup()
                .addComponent(jLabel2)
                .addGap(0, 0, 0)
                .addComponent(pnlDocenti2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel6.setText("Programma:");

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setOpaque(false);
        jScrollPane1.setPreferredSize(new java.awt.Dimension(0, 0));
        jScrollPane1.setWheelScrollingEnabled(false);

        txtProgramma2.setEditable(false);
        txtProgramma2.setLineWrap(true);
        txtProgramma2.setTabSize(4);
        txtProgramma2.setText("PROGRAMMA");
        txtProgramma2.setWrapStyleWord(true);
        txtProgramma2.setBorder(null);
        txtProgramma2.setOpaque(false);
        jScrollPane1.setViewportView(txtProgramma2);

        javax.swing.GroupLayout pnlProgrammaLayout = new javax.swing.GroupLayout(pnlProgramma);
        pnlProgramma.setLayout(pnlProgrammaLayout);
        pnlProgrammaLayout.setHorizontalGroup(
            pnlProgrammaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlProgrammaLayout.createSequentialGroup()
                .addComponent(jLabel6)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(pnlProgrammaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlProgrammaLayout.setVerticalGroup(
            pnlProgrammaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlProgrammaLayout.createSequentialGroup()
                .addComponent(jLabel6)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout pnlMenuLayout = new javax.swing.GroupLayout(pnlMenu);
        pnlMenu.setLayout(pnlMenuLayout);
        pnlMenuLayout.setHorizontalGroup(
            pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMenuLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlMenuLayout.createSequentialGroup()
                        .addComponent(lnkBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(lnkAddLezione, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlMenuLayout.createSequentialGroup()
                        .addGroup(pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(chkSoloPresenza, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(pnlTimes, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlIscritti, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlDocenti, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlProgramma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        pnlMenuLayout.setVerticalGroup(
            pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMenuLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lnkBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lnkAddLezione, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(chkSoloPresenza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(pnlTimes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(pnlIscritti, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(pnlDocenti, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(pnlProgramma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(12, 12, 12))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblTitolare)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlMenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lblTitolare))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addComponent(pnlMenu, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void lnkBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lnkBackMouseClicked
        CONTROLLER.back();
    }//GEN-LAST:event_lnkBackMouseClicked

    private void chkSoloPresenzaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkSoloPresenzaActionPerformed
        boolean chk = chkSoloPresenza.isSelected();
        for (Component c : lstLezioni.getComponents()) {
            if (!(c instanceof JViewLezione))
                continue;
            boolean pres = (((JViewLezione)c).lez.tipo == TipoLezione.PRESENZA);
            c.setVisible(!chk || pres);
        }
    }//GEN-LAST:event_chkSoloPresenzaActionPerformed

    private void lnkAddLezioneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lnkAddLezioneMouseClicked
        CONTROLLER.editLesson(new LezionePresenza(
            null, "Nuova lezione",
            ins, (Docente)CONTROLLER.getPersona(),
            0, LocalDateTime.now()
        ), true);
    }//GEN-LAST:event_lnkAddLezioneMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox chkSoloPresenza;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblIscritti;
    private javax.swing.JLabel lblTitolare;
    private it.smartteaching.prototype.view.widgets.JLink lnkAddLezione;
    private it.smartteaching.prototype.view.widgets.JLink lnkBack;
    private javax.swing.JPanel lstLezioni;
    private javax.swing.JLabel numOreTot;
    private javax.swing.JLabel numRemaining;
    private javax.swing.JLabel numStreaming;
    private javax.swing.JPanel pnlDocenti;
    private javax.swing.JPanel pnlDocenti2;
    private javax.swing.JPanel pnlIscritti;
    private javax.swing.JPanel pnlIscritti2;
    private javax.swing.JPanel pnlMenu;
    private javax.swing.JPanel pnlProgramma;
    private javax.swing.JPanel pnlTimes;
    private javax.swing.JTextArea txtProgramma2;
    // End of variables declaration//GEN-END:variables
}
