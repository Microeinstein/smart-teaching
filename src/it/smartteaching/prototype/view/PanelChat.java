package it.smartteaching.prototype.view;

import it.smartteaching.prototype.Core;
import static it.smartteaching.prototype.model.Model.MODEL;
import static it.smartteaching.prototype.model.classiChat.*;
import it.smartteaching.prototype.model.classiPersone.Persona;
import java.awt.event.KeyEvent;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.DefaultListModel;
import javax.swing.ListSelectionModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import static it.smartteaching.prototype.control.Controller.CONTROLLER;

public class PanelChat extends PanelCommon {
    public static class ChatItem {
        public final Chat chat;
        public final Persona first_other;
        public final String label;
        public ChatItem(Chat c) {
            this.chat = c;
            first_other = c.getOthers().get(0);
            this.label = c.getLabel();
        }
        @Override
        public String toString() {
            return label;
        }
    }
    private final Persona initialp;
    private final Chat initial;
    private ChatItem selected;
    private boolean moving = false;
    public final DefaultListModel itemList;
    public final ListSelectionModel itemSelection;

    public PanelChat(Persona p) {
        super("Chat", "Chat");
        initComponents();
        this.initialp = p;
        this.initial = p == null? null : MODEL.apriChatPrivata(CONTROLLER.getPersona(), p);
        this.selected = null;
        itemList = new DefaultListModel();
        lstRecents.setModel(itemList);
        itemSelection = lstRecents.getSelectionModel();
        itemSelection.addListSelectionListener(l -> showSelectedChat());
        loadChats();
    }
    private void loadChats() {
        List<Chat> lst = MODEL.getRecenti(CONTROLLER.getPersona());
        itemSelection.clearSelection();
        itemList.clear();
        int priv_i = -1;
        int i = -1;
        for (Chat c : lst) {
            i++;
            ChatItem ci = new ChatItem(c);
            itemList.addElement(ci);
            if (initial != null && c.tipo == TipoChat.PRIVATO && ci.chat.equals(initial))
                priv_i = i;
        }
        if (priv_i >= 0)
            itemSelection.setSelectionInterval(priv_i, priv_i);
        else if (initial != null)
            newRecentChat(initial);
        else
            reactSelection(false);
    }
    private void newRecentChat(Chat c) {
        ChatItem ci = new ChatItem(c);
        itemList.insertElementAt(ci, 0);
        itemSelection.setSelectionInterval(0, 0);
        setRecenti();
    }
    private void selectChat(Chat c) {
        ChatItem ci;
        int i = 0;
        for (Object obj : itemList.toArray()) {
            ci = (ChatItem)obj;
            if (ci.chat.equals(c)) {
                itemSelection.setSelectionInterval(i, i);
                return;
            }
            i++;
        }
        newRecentChat(c);
    }
    private void moveSelectionToTop() {
        int[] in = itemSelection.getSelectedIndices();
        boolean isSel = in.length > 0;
        if (!isSel)
            return;
        int[] sel = Arrays.stream(in)
            .filter(i -> i >= 0 && i < itemList.size())
            .toArray();
        if (sel.length < 1)
            return;
        int isel = sel[0];
        moving = true;
        Object objsel = itemList.get(isel);
        itemList.remove(isel);
        itemList.insertElementAt(objsel, 0);
        itemSelection.setSelectionInterval(0, 0);
        setRecenti();
        moving = false;
    }
    
    private void reactSelection(boolean mode) {
        txtMsg.setEnabled(mode);
        lblLastVisit.setText("");
        lblLastVisit.setVisible(mode);
        lnkPartecipanti.setVisible(false);
        if (!mode)
            lblNome.setText("Seleziona una chat");
    }
    private void showSelectedChat() {
        this.lstRecents.revalidate();
        this.lstRecents.repaint();
        if (moving)
            return;
        int[] in = itemSelection.getSelectedIndices();
        boolean isSel = in.length > 0;
        reactSelection(isSel);
        if (!isSel)
            return;
        ChatItem[] sel = Arrays.stream(in)
            .filter(i -> i >= 0 && i < itemList.size())
            .mapToObj(i -> itemList.get(i))
            .toArray(ChatItem[]::new);
        if (sel.length < 1)
            return;
        selected = sel[0];
        showChat();
    }
    private void showChat() {
        txtMsg.setEnabled(true);
        lblNome.setText(selected.label);
        txtConv.setText("");
        boolean priv = selected.chat.tipo == TipoChat.PRIVATO;
        boolean broad = selected.chat.tipo == TipoChat.BROADCAST;
        lnkPartecipanti.setVisible(broad);
        lblLastVisit.setVisible(priv);
        if (priv)
            lblLastVisit.setText(selected.first_other.getLastVisitMsg());
        for (Messaggio m : selected.chat.getMessaggi())
            appendMessage(m);
    }
    
    private void appendMessage(Messaggio m) {
        if (txtConv.getDocument().getLength() > 0)
            append("\n");
        append(String.format("[%s] %s: %s",
            Core.CustomDateTimeFormat.format(m.data),
            m.mittente.nome,
            m.contenuto
        ));
    }
    private void append(String s) {
        try {
            Document doc = txtConv.getDocument();
            doc.insertString(doc.getLength(), s, null);
        } catch(BadLocationException exc) {
            exc.printStackTrace();
        }
    }
    private void send() {
        String cont = txtMsg.getText();
        Messaggio m = new Messaggio(CONTROLLER.getPersona(), cont, LocalDateTime.now());
        selected.chat.mandaMessaggio(m);
        txtMsg.setText("");
        appendMessage(m);
        moveSelectionToTop();
    }
    private void setRecenti() {
        Object[] arr = new Object[itemList.size()];
        itemList.copyInto(arr);
        MODEL.setRecenti(CONTROLLER.getPersona(), Arrays.stream(arr)
            .map(o -> ((ChatItem)o).chat)
            .collect(Collectors.toList())
        );
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        pnlMenu = new javax.swing.JPanel();
        lnkBack = new it.smartteaching.prototype.view.widgets.JLink();
        lnkNewChat = new it.smartteaching.prototype.view.widgets.JLink();
        jScrollPane3 = new javax.swing.JScrollPane();
        lstRecents = new javax.swing.JList<>();
        lnkNewChatBroadcast = new it.smartteaching.prototype.view.widgets.JLink();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtMsg = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtConv = new javax.swing.JTextPane();
        lblNome = new javax.swing.JLabel();
        lblLastVisit = new javax.swing.JLabel();
        lnkPartecipanti = new it.smartteaching.prototype.view.widgets.JLink();

        jLabel1.setText("$HEADER");

        pnlMenu.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 0, 1, FormCommon.BORDER_COLOR));
        pnlMenu.setMinimumSize(new java.awt.Dimension(210, 210));
        pnlMenu.setPreferredSize(new java.awt.Dimension(210, 210));

        lnkBack.setText("Indietro");
        lnkBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkBackMouseClicked(evt);
            }
        });

        lnkNewChat.setText("Nuova chat");
        lnkNewChat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkNewChatMouseClicked(evt);
            }
        });

        lstRecents.setBorder(null);
        lstRecents.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane3.setViewportView(lstRecents);

        lnkNewChatBroadcast.setText("Nuova chat di broadcast");
        lnkNewChatBroadcast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkNewChatBroadcastMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout pnlMenuLayout = new javax.swing.GroupLayout(pnlMenu);
        pnlMenu.setLayout(pnlMenuLayout);
        pnlMenuLayout.setHorizontalGroup(
            pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(pnlMenuLayout.createSequentialGroup()
                .addGroup(pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlMenuLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(lnkBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lnkNewChat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21))
                    .addGroup(pnlMenuLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lnkNewChatBroadcast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(44, Short.MAX_VALUE))
        );
        pnlMenuLayout.setVerticalGroup(
            pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMenuLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lnkBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lnkNewChat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lnkNewChatBroadcast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3))
        );

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setAutoscrolls(true);
        jScrollPane1.setMaximumSize(new java.awt.Dimension(32767, 32));
        jScrollPane1.setMinimumSize(new java.awt.Dimension(28, 32));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(242, 32));
        jScrollPane1.setRequestFocusEnabled(false);
        jScrollPane1.setViewportView(null);

        txtMsg.setLineWrap(true);
        txtMsg.setRows(1);
        txtMsg.setTabSize(4);
        txtMsg.setWrapStyleWord(true);
        txtMsg.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, FormCommon.BORDER_COLOR));
        txtMsg.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMsgKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(txtMsg);

        jScrollPane2.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 0, 1, 0, FormCommon.BORDER_COLOR));

        txtConv.setEditable(false);
        jScrollPane2.setViewportView(txtConv);

        lblNome.setFont(new java.awt.Font("Inter", 1, 13)); // NOI18N
        lblNome.setText("NOME");

        lblLastVisit.setText("ultimo accesso:");

        lnkPartecipanti.setText("partecipanti");
        lnkPartecipanti.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkPartecipantiMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 525, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addComponent(lblNome)
                        .addGap(18, 18, 18)
                        .addComponent(lnkPartecipanti, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblLastVisit)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNome)
                    .addComponent(lblLastVisit)
                    .addComponent(lnkPartecipanti, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlMenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlMenu, javax.swing.GroupLayout.DEFAULT_SIZE, 437, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void lnkBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lnkBackMouseClicked
        CONTROLLER.back();
    }//GEN-LAST:event_lnkBackMouseClicked

    private void lnkNewChatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lnkNewChatMouseClicked
        DialogNewChat d = new DialogNewChat();
        d.setVisible(true);
        if (!d.isApproved())
            return;
        Persona p2 = d.applyData();
        Chat c = MODEL.apriChatPrivata(CONTROLLER.getPersona(), p2);
        if (c == null) //chat aperta con se stesso
            return;
        selectChat(c);
    }//GEN-LAST:event_lnkNewChatMouseClicked

    private void txtMsgKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMsgKeyPressed
        int k = evt.getKeyCode();
        int m = evt.getModifiersEx();
        if (k == KeyEvent.VK_ENTER) {
            if ((m & KeyEvent.SHIFT_DOWN_MASK) == 0) {
                send();
                evt.consume();
            } else { //shift+enter
            }
        }
    }//GEN-LAST:event_txtMsgKeyPressed

    private void lnkNewChatBroadcastMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lnkNewChatBroadcastMouseClicked
        DialogNewChatBroadcast d = new DialogNewChatBroadcast();
        d.setVisible(true);
        if (!d.isApproved())
            return;
        ChatBroadcast cb = d.applyData();
        selectChat(cb);
    }//GEN-LAST:event_lnkNewChatBroadcastMouseClicked

    private void lnkPartecipantiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lnkPartecipantiMouseClicked
        DialogViewPartecipanti d = new DialogViewPartecipanti(selected.chat);
        d.setVisible(true);
    }//GEN-LAST:event_lnkPartecipantiMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblLastVisit;
    private javax.swing.JLabel lblNome;
    private it.smartteaching.prototype.view.widgets.JLink lnkBack;
    private it.smartteaching.prototype.view.widgets.JLink lnkNewChat;
    private it.smartteaching.prototype.view.widgets.JLink lnkNewChatBroadcast;
    private it.smartteaching.prototype.view.widgets.JLink lnkPartecipanti;
    private javax.swing.JList<String> lstRecents;
    private javax.swing.JPanel pnlMenu;
    private javax.swing.JTextPane txtConv;
    private javax.swing.JTextArea txtMsg;
    // End of variables declaration//GEN-END:variables
}
