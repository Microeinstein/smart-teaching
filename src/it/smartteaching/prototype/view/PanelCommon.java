package it.smartteaching.prototype.view;

import static it.smartteaching.prototype.Core.setSameFontSize;
import static it.smartteaching.prototype.view.FormCommon.BG_COLOR;
import java.awt.Component;
import javax.swing.*;

public class PanelCommon extends javax.swing.JPanel {
    final String title, header;

    public PanelCommon(String title, String header) {
        this.title = title;
        this.header = header;
        initComponents();
    }

    public void setNewDefaults(Component c) {
        //System.err.println(c.getClass().getName());
        if (!(c instanceof JTabbedPane))
            c.setBackground(BG_COLOR);
        if (c instanceof JLabel) {
            JLabel l = ((JLabel)c);
            if (l.getText().equals("$HEADER")) {
                l.setText(header);
                setSameFontSize(l, 20);
            }
        } else if (c instanceof JTextArea) {
            ((JTextArea) c).setOpaque(false);
        } else if (c instanceof JPanel) {
            for (Component c2 : ((JPanel)c).getComponents())
                setNewDefaults(c2);
        } else if (c instanceof JScrollPane) {
            ((JScrollPane)c).setOpaque(false);
            JViewport vp = ((JScrollPane)c).getViewport();
            vp.setBackground(BG_COLOR);
            vp.setOpaque(false);
            for (Component c2 : vp.getComponents())
                setNewDefaults(c2);
        } else if (c instanceof JTabbedPane) {
            ((JTabbedPane) c).setBackgroundAt(0, BG_COLOR);
            //for (Component c2 : ((JTabbedPane)c).getComponents())
            //    setNewDefaults(c2);
        }
    }
    public void applyStyle() {
        this.setBackground(BG_COLOR);
        for (Component c : this.getComponents())
            setNewDefaults(c);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
