package it.smartteaching.prototype.view;

import it.smartteaching.prototype.Core.IDataUserEditor;
import it.smartteaching.prototype.model.Insegnamento;
import it.smartteaching.prototype.model.Model;
import static it.smartteaching.prototype.model.Model.MODEL;
import it.smartteaching.prototype.model.classiPersone.Persona;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import javax.swing.DefaultListModel;
import javax.swing.ListSelectionModel;
import static it.smartteaching.prototype.control.Controller.CONTROLLER;

public class DialogNewChat extends javax.swing.JDialog implements IDataUserEditor<Persona> {
    private boolean ok = false;
    public final DefaultListModel itemList;
    public final ListSelectionModel itemSelection;
    
    public DialogNewChat() {
        super(CONTROLLER.getForm(), ModalityType.APPLICATION_MODAL);
        initComponents();
        itemList = new DefaultListModel();
        lstDest.setModel(itemList);
        itemSelection = lstDest.getSelectionModel();
        loadEntries();
        this.setLocationRelativeTo(CONTROLLER.getForm());
    }
    private void loadEntries() {
        Persona p = CONTROLLER.getPersona();
        Set<Persona> pers = new TreeSet<>((a, b) -> a.compareTo(b));
        for (Insegnamento ins : MODEL.getInsegnamenti(p, true)) {
            ins.getIscritti().forEach(p2 -> pers.add(p2));
            ins.getDocenti().forEach(p2 -> pers.add(p2));
        }
        itemList.addAll(pers);
    }

    @Override
    public boolean isApproved() {
        return ok;
    }
    @Override
    public Model.DataOperation getChoice() {
        return Model.DataOperation.ADD;
    }
    @Override
    public Persona applyData() {
        int[] in = itemSelection.getSelectedIndices();
        boolean isSel = in.length > 0;
        if (!isSel)
            return null;
        List<Persona> sel = Arrays.stream(in)
            .filter(i -> i >= 0 && i < itemList.size())
            .mapToObj(i -> (Persona)itemList.get(i))
            .collect(Collectors.toList());
        if (sel.size() < 1)
            return null;
        return sel.get(0);
    }
    @Override
    public void undoPending() {}

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        lstDest = new javax.swing.JList<>();
        jLabel3 = new javax.swing.JLabel();
        btnOk = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Nuova chat");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        lstDest.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(lstDest);

        jLabel3.setText("Seleziona destinatario:");

        btnOk.setText("Apri");
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        btnCancel.setText("Annulla");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 88, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 237, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        ok = true;
        this.setVisible(false);
    }//GEN-LAST:event_btnOkActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        ok = false;
        this.setVisible(false);
    }//GEN-LAST:event_btnCancelActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        ok = false;
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnOk;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<String> lstDest;
    // End of variables declaration//GEN-END:variables
}
