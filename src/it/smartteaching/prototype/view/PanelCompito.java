package it.smartteaching.prototype.view;

import it.smartteaching.prototype.Core;
import static it.smartteaching.prototype.model.Model.MODEL;
import it.smartteaching.prototype.model.Model;
import it.smartteaching.prototype.model.Model.*;
import it.smartteaching.prototype.model.classiFile.*;
import it.smartteaching.prototype.model.classiPersone.Studente;
import it.smartteaching.prototype.view.widgets.JEditFiles;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import org.eclipse.swt.widgets.DirectoryDialog;
import static it.smartteaching.prototype.control.Controller.CONTROLLER;

public class PanelCompito extends PanelCommon {
    private final Compito com;
    private Consegna con;
    private DataOperation op;
    public final DefaultListModel itemList;
    public final ListSelectionModel itemSelection;
    private final DirectoryDialog dirdialog = new DirectoryDialog(CONTROLLER.getForm().shell);

    public PanelCompito(Compito c) {
        super(c.nome, c.lezione.insegnamento.nome);
        initComponents();
        this.com = c;
        itemList = new DefaultListModel();
        lstSubmissions.setModel(itemList);
        itemSelection = lstSubmissions.getSelectionModel();
        itemSelection.addListSelectionListener(l -> showSelectedSubmission());
        dirdialog.setText("Seleziona cartella downloads");
        loadData();
    }
    private void loadData() {
        lblTitolare.setText("titolare: " + com.lezione.insegnamento.titolare.nome);
        lezHeader.showData(com.lezione);
        op = DataOperation.UPDATE;
        boolean docente = CONTROLLER.isDocente();
        lblComNome.setText(this.com.nome);
        String txtAssegn = Core.CustomDateTimeFormat.format(this.com.ultimaModifica);
        lblComAssegnato.setText(String.format("(%s)", txtAssegn));
        String txtScad = Core.CustomDateTimeFormat.format(this.com.scadenza);
        lblComScadenza.setText("→" + txtScad);
        txtDesc.setText(this.com.descrizione);
        filesEditor.editor.selectionHandler = this::showFilesInfo;
        btnAccept.setText(docente? "Conferma" : "Consegna");
        filesEditor.setReadOnly(docente);
        if (!docente) {
            btnAccept.addActionListener(evt -> submit());
            Core.removeComp(lstSubmissions);
            Core.removeComp(lnkDelMark);
            Core.removeComp(lblNewMark);
            Core.removeComp(lblNewMax);
            Core.removeComp(numMark);
            getSelfSubmission();
        } else {
            btnAccept.addActionListener(evt -> setMark());
            itemList.clear();
            this.com.getConsegneCaricate().forEach((s,c) -> {
                itemList.addElement(c);
            });
            itemSelection.setSelectionInterval(0, 0);
            lnkDelMark.addClickHandler(evt -> delMark());
        }
    }
    private void getSelfSubmission() {
        Studente s = (Studente)CONTROLLER.getPersona();
        con = this.com.getConsegna(s);
        if (con == null) {
            con = new Consegna(com, s, new ArrayList<>(), null, -1);
            op = DataOperation.ADD;
        }
        showSubmission();
    }
    private void showSelectedSubmission() {
        this.lstSubmissions.revalidate();
        this.lstSubmissions.repaint();
        int[] in = itemSelection.getSelectedIndices();
        boolean isSel = in.length > 0;
        lblNewMark.setEnabled(isSel);
        numMark.setEnabled(isSel);
        btnDownload.setEnabled(isSel);
        btnAccept.setEnabled(isSel);
        if (!isSel) {
            lnkDelMark.setEnabled(isSel);
            return;
        }
        Consegna[] sel = Arrays.stream(in)
            .filter(i -> i >= 0 && i < itemList.size())
            .mapToObj(i -> itemList.get(i))
            .toArray(Consegna[]::new);
        if (sel.length < 1)
            return;
        con = sel[0];
        lnkDelMark.setEnabled(this.con.valutazione >= 0);
        showSubmission();
    }
    private void showSubmission() {
        boolean docente = CONTROLLER.isDocente(),
                consegnato = this.con.consegna != null,
                valutato = this.con.valutazione >= 0;
        lblSubmitDate.setText(consegnato? Core.CustomDateTimeFormat.format(this.con.consegna): "N/A");
        lblMark.setText(valutato? this.con.valutazione+"/100" : "N/A");
        filesEditor.loadFiles(con.caricati);
        filesEditor.setReadOnly(docente || valutato);
        btnAccept.setEnabled(docente || !valutato);
        if (docente && valutato)
            numMark.setValue(this.con.valutazione);
    }
    private void showFilesInfo() {
        boolean isSel = JEditFiles.showFilesInfo(filesEditor.editor, lblSize, lblMIME);
        btnDownload.setEnabled(isSel);
    }

    private void submit() {
        con.caricati = filesEditor.getFiles();
        con.consegna = LocalDateTime.now();
        MODEL.modificaConsegna(con, op);
        getSelfSubmission();
    }
    private void setMark() {
        con.valutazione = (int)numMark.getValue();
        MODEL.modificaConsegna(con, op);
        showSelectedSubmission();
    }
    private void delMark() {
        con.valutazione = -1;
        MODEL.modificaConsegna(con, op);
        showSelectedSubmission();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        lblTitolare = new javax.swing.JLabel();
        pnlMenu = new javax.swing.JPanel();
        lnkBack = new it.smartteaching.prototype.view.widgets.JLink();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lblSize = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblMIME = new javax.swing.JLabel();
        lblMark = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lblSubmitDate = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        lnkDelMark = new it.smartteaching.prototype.view.widgets.JLink();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstSubmissions = new javax.swing.JList<>();
        jPanel1 = new javax.swing.JPanel();
        lezHeader = new it.smartteaching.prototype.view.widgets.JViewHeaderLezione();
        lblComNome = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDesc = new javax.swing.JTextArea();
        lblComScadenza = new javax.swing.JLabel();
        lblComAssegnato = new javax.swing.JLabel();
        filesEditor = new it.smartteaching.prototype.view.widgets.JEditFiles();
        btnAccept = new javax.swing.JButton();
        btnDownload = new javax.swing.JButton();
        lblNewMark = new javax.swing.JLabel();
        numMark = new javax.swing.JSpinner();
        lblNewMax = new javax.swing.JLabel();

        jLabel1.setText("$HEADER");

        lblTitolare.setText("TITOLARE");

        pnlMenu.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 0, 1, FormCommon.BORDER_COLOR));
        pnlMenu.setMinimumSize(new java.awt.Dimension(210, 210));
        pnlMenu.setPreferredSize(new java.awt.Dimension(210, 210));

        lnkBack.setText("Indietro");
        lnkBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkBackMouseClicked(evt);
            }
        });

        jPanel2.setPreferredSize(new java.awt.Dimension(195, 180));

        jLabel2.setText("Dimensione:");

        lblSize.setFont(new java.awt.Font("JetBrains Mono", 0, 13)); // NOI18N
        lblSize.setText("0B");

        jLabel4.setText("Tipo:");

        lblMIME.setFont(new java.awt.Font("JetBrains Mono", 0, 13)); // NOI18N
        lblMIME.setText("text/plain");

        lblMark.setText("N/A");

        jLabel5.setText("Voto assegnato:");

        lblSubmitDate.setText("N/A");

        jLabel7.setText("Consegnato il:");

        lnkDelMark.setText("cancella");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lnkDelMark, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jSeparator1))
                .addGap(0, 0, 0))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lblMark, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addComponent(lblSize, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblMIME, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblSubmitDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel7)
                .addGap(0, 0, 0)
                .addComponent(lblSubmitDate)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(lnkDelMark, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addComponent(lblMark)
                .addGap(8, 8, 8)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(jLabel2)
                .addGap(0, 0, 0)
                .addComponent(lblSize)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4)
                .addGap(0, 0, 0)
                .addComponent(lblMIME)
                .addGap(0, 0, 0))
        );

        jScrollPane2.setPreferredSize(new java.awt.Dimension(185, 130));

        lstSubmissions.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, FormCommon.BORDER_COLOR));
        lstSubmissions.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(lstSubmissions);

        javax.swing.GroupLayout pnlMenuLayout = new javax.swing.GroupLayout(pnlMenu);
        pnlMenu.setLayout(pnlMenuLayout);
        pnlMenuLayout.setHorizontalGroup(
            pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMenuLayout.createSequentialGroup()
                .addGroup(pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlMenuLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(lnkBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 136, Short.MAX_VALUE))
                    .addGroup(pnlMenuLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE))))
                .addContainerGap())
        );
        pnlMenuLayout.setVerticalGroup(
            pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMenuLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(lnkBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
                .addContainerGap())
        );

        lblComNome.setText("COMPITO");

        txtDesc.setEditable(false);
        txtDesc.setColumns(20);
        txtDesc.setTabSize(4);
        txtDesc.setText("Sì");
        txtDesc.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, FormCommon.BORDER_COLOR), javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        txtDesc.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtDesc.setMargin(new java.awt.Insets(5, 5, 5, 5));
        jScrollPane1.setViewportView(txtDesc);

        lblComScadenza.setText("SCADENZA");
        lblComScadenza.setToolTipText("Data di scadenza");

        lblComAssegnato.setText("ASSEGNATO");
        lblComAssegnato.setToolTipText("Data di assegnazione");

        filesEditor.setInfoHidden(true);

        btnAccept.setText("ACCEPT");

        btnDownload.setText("Scarica");
        btnDownload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownloadActionPerformed(evt);
            }
        });

        lblNewMark.setText("Nuovo voto:");

        numMark.setModel(new javax.swing.SpinnerNumberModel(50, 0, 100, 1));

        lblNewMax.setText("/100");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lezHeader, javax.swing.GroupLayout.DEFAULT_SIZE, 531, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblComNome)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblComAssegnato)
                        .addGap(18, 18, 18)
                        .addComponent(lblComScadenza))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 531, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(btnDownload)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblNewMark)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(numMark, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(lblNewMax)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAccept))
                    .addComponent(filesEditor, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lezHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblComNome)
                    .addComponent(lblComScadenza)
                    .addComponent(lblComAssegnato))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(filesEditor, javax.swing.GroupLayout.DEFAULT_SIZE, 303, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAccept, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDownload, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNewMark)
                    .addComponent(numMark, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNewMax)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblTitolare)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlMenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lblTitolare))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlMenu, javax.swing.GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void lnkBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lnkBackMouseClicked
        CONTROLLER.back();
    }//GEN-LAST:event_lnkBackMouseClicked

    private void btnDownloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownloadActionPerformed
        String dir = dirdialog.open();
        if (dir == null)
            return;
        Path p = Path.of(dir);
        int[] in = filesEditor.editor.itemSelection.getSelectedIndices();
        String failed = Arrays.stream(in)
            .filter(i -> i >= 0 && i < filesEditor.editor.itemList.size())
            .mapToObj(i -> filesEditor.editor.itemList.get(i))
            .filter(f -> f instanceof FileRemoto)
            .map(f -> {
                FileRemoto fr = (FileRemoto)f;
                return new Core.Pair<FileRemoto, Boolean>(fr, fr.scaricaIn(p));
            })
            .filter(pp -> !pp.snd)
            .map(pp -> pp.fst.nome)
            .collect(Collectors.joining("\n"));
        if (!failed.isEmpty()) {
            JOptionPane.showMessageDialog(
                this,
                "Impossibile scaricare i seguenti files:\n" + failed,
                "Smart Teaching",
                JOptionPane.WARNING_MESSAGE
            );
        }
    }//GEN-LAST:event_btnDownloadActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAccept;
    private javax.swing.JButton btnDownload;
    private it.smartteaching.prototype.view.widgets.JEditFiles filesEditor;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblComAssegnato;
    private javax.swing.JLabel lblComNome;
    private javax.swing.JLabel lblComScadenza;
    private javax.swing.JLabel lblMIME;
    private javax.swing.JLabel lblMark;
    private javax.swing.JLabel lblNewMark;
    private javax.swing.JLabel lblNewMax;
    private javax.swing.JLabel lblSize;
    private javax.swing.JLabel lblSubmitDate;
    private javax.swing.JLabel lblTitolare;
    private it.smartteaching.prototype.view.widgets.JViewHeaderLezione lezHeader;
    private it.smartteaching.prototype.view.widgets.JLink lnkBack;
    private it.smartteaching.prototype.view.widgets.JLink lnkDelMark;
    private javax.swing.JList<String> lstSubmissions;
    private javax.swing.JSpinner numMark;
    private javax.swing.JPanel pnlMenu;
    private javax.swing.JTextArea txtDesc;
    // End of variables declaration//GEN-END:variables
}
