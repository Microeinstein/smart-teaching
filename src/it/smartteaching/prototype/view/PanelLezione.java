package it.smartteaching.prototype.view;

import static it.smartteaching.prototype.Core.*;
import it.smartteaching.prototype.model.Insegnamento;
import it.smartteaching.prototype.model.Model.DataOperation;
import it.smartteaching.prototype.model.classiConferencePlatform.DummyPrenotazione;
import it.smartteaching.prototype.model.classiConferencePlatform.IPrenotazione;
import it.smartteaching.prototype.model.classiFile.Compito;
import it.smartteaching.prototype.model.classiFile.Materiale;
import it.smartteaching.prototype.model.classiLezione.*;
import java.awt.Dimension;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JOptionPane;
import static it.smartteaching.prototype.control.Controller.CONTROLLER;

public class PanelLezione extends PanelCommon implements IDataUserEditor<Lezione> {
    public static final String
        CHANGES_MATERIAL = "materiali",
        CHANGES_ASSIGN = "compiti";
    public final Insegnamento ins;
    private Lezione lez;
    private boolean nuovo, ok;
    private DataOperation op;
    private TipoLezione cust_type;
    private IPrenotazione cust_link = null;
    private Changes<Materiale> editsMat = new Changes<>();
    private Changes<Compito> editsAssign = new Changes<>();

    public PanelLezione(Lezione lez, boolean nuovo) {
        super("Modifica", lez.insegnamento.nome);
        this.nuovo = nuovo;
        this.ins = lez.insegnamento;
        this.lez = lez;
        initComponents();
        editorMat.lblType.setText("Materiali:");
        editorAssign.lblType.setText("Compiti:");
        editorMat.lblType.setMinimumSize(new Dimension(60, editorMat.lblType.getHeight()));
        editorAssign.lblType.setMinimumSize(new Dimension(60, editorMat.lblType.getHeight()));

        editorMat.editHandler = (i, o) -> {
            Materiale m = (Materiale)o;
            DialogMateriale dlg = new DialogMateriale(m);
            dlg.setVisible(true);
            if (!dlg.isApproved())
                return;
            editsMat.perform(dlg.applyData(), DataOperation.UPDATE);
            editorMat.itemList.setElementAt(m, i);
        };
        editorMat.jAddRem.btnAdd.addActionListener(ae -> {
            Materiale m = new Materiale(genid(), lez, null, "Nuovo materiale");
            DialogMateriale dlg = new DialogMateriale(m);
            dlg.setVisible(true);
            if (!dlg.isApproved())
                return;
            editsMat.perform(dlg.applyData(), DataOperation.ADD);
            editorMat.itemList.addElement(m);
        });

        editorAssign.editHandler = (i, o) -> {
            Compito c = (Compito)o;
            DialogCompito dlg = new DialogCompito(c);
            dlg.setVisible(true);
            if (!dlg.isApproved())
                return;
            editsAssign.perform(dlg.applyData(), DataOperation.UPDATE);
            editorAssign.itemList.setElementAt(c, i);
        };
        editorAssign.jAddRem.btnAdd.addActionListener(ae -> {
            Compito c = new Compito(genid(), lez,
                "Nuovo compito", null,
                LocalDateTime.now(), LocalDateTime.now()
            );
            DialogCompito dlg = new DialogCompito(c);
            dlg.setVisible(true);
            if (!dlg.isApproved())
                return;
            editsAssign.perform(dlg.applyData(), DataOperation.ADD);
            editorAssign.itemList.addElement(c);
        });
        loadData();
    }

    private void loadData() {
        txtName.setText(lez.nome);
        txtDate.setText(CustomDateTimeFormat.format(lez.data));
        this.cust_type = lez.tipo;
        if (lez.tipo == TipoLezione.STREAMING) {
            LezioneStreaming ls = (LezioneStreaming)lez;
            this.cust_link = ls.link;
        }
        cmbType.setSelectedIndex(cust_type.ordinal());
        btnRemove.setVisible(!nuovo);
        editorMat.itemList.addAll(lez.getMateriali());
        editorAssign.itemList.addAll(lez.getCompiti());
        changeLessonType();
        updateBookState();
    }
    private TipoLezione getNewType() {
        return TipoLezione.values()[cmbType.getSelectedIndex()];
    }
    private LocalDateTime getNewDate() {
        return LocalDateTime.parse(txtDate.getText(), CustomDateTimeFormat);
    }
    private void changeLessonType() {
        pnlBook.setVisible(cust_type == TipoLezione.STREAMING);
        txtDuration.setEnabled(cust_type != TipoLezione.SOLO_MATERIALE);
        if (cust_type == TipoLezione.SOLO_MATERIALE)
            txtDuration.setText("00:00");
        else
            timeSet(lez.durata, (h,m) -> {
                txtDuration.setText(String.format("%02d:%02d", h, m));
            });
    }
    private void updateBookState() {
        boolean set = this.cust_link != null;
        lnkBook.setEnabled(set);
        lnkBook.setText(set? "impostato" : "mancante");
        lnkBook.setToolTipText(set? this.cust_link.getLink() : null);
        btnBook.setEnabled(!set);
        btnBookFree.setEnabled(set);
    }
    private boolean checkLessonConsistent() {
        if (cust_type == TipoLezione.STREAMING) {
            if (cust_link == null) {
                JOptionPane.showMessageDialog(
                    this,
                    "Una lezione in streaming deve avere una prenotazione.",
                    "Smart Teaching",
                    JOptionPane.WARNING_MESSAGE
                );
                return false;
            }
        }
        if (cust_type == TipoLezione.SOLO_MATERIALE) {
            if (editorMat.itemList.size() < 1) {
                JOptionPane.showMessageDialog(
                    this,
                    "Una lezione con solo materiali deve avere almeno del materiale.",
                    "Smart Teaching",
                    JOptionPane.WARNING_MESSAGE
                );
                return false;
            }
        }
        return true;
    }
    private void askBooking() {
        this.cust_link = CONTROLLER.askBooking(getNewDate());
        if (this.cust_link == null) {
            JOptionPane.showMessageDialog(
                this,
                "Prenotazione non riuscita, prova a cambiare data.",
                "Smart Teaching",
                JOptionPane.ERROR_MESSAGE
            );
        }
        updateBookState();
    }
    private void freeBooking() {
        CONTROLLER.freeBooking(this.cust_link);
        this.cust_link = null;
        updateBookState();
    }

    @Override
    public boolean isApproved() {
        return ok;
    }

    @Override
    public DataOperation getChoice() {
        return op;
    }

    @Override
    public Lezione applyData() {
        String name = this.txtName.getText();
        LocalDateTime date = getNewDate();
        List<Integer> dur = Arrays.stream(txtDuration.getText().split(":"))
            .map(s -> Integer.parseInt(s))
            .collect(Collectors.toList());
        int duration = dur.get(1) + dur.get(0) * 60;
        String id = nuovo? genid() : lez.id;
        switch (cust_type) {
            case PRESENZA:
                lez = new LezionePresenza(
                    id, name,
                    lez.insegnamento, lez.docente,
                    duration, date
                );
                break;
            case STREAMING:
                lez = new LezioneStreaming(
                    id, name,
                    lez.insegnamento, lez.docente,
                    duration, date, (DummyPrenotazione)cust_link
                );
                break;
            case SOLO_MATERIALE:
                lez = new LezioneSoloMateriale(
                    id, name,
                    lez.insegnamento, lez.docente, date
                );
                break;
            default:
                throw new NoClassDefFoundError(cust_type.name());
        }
        return lez;
    }

    @Override
    public Changes<?> getChanges(String name) {
        if (name.equals(CHANGES_MATERIAL))
            return editsMat;
        else if (name.equals(CHANGES_ASSIGN))
            return editsAssign;
        return IDataUserEditor.super.getChanges(name);
    }

    @Override
    public void undoPending() {
        if (cust_type == TipoLezione.STREAMING) {
            if (this.cust_link != null && (
                !(lez instanceof LezioneStreaming) ||
                !this.cust_link.equals(((LezioneStreaming)lez).link)
            ))
                freeBooking();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitle = new javax.swing.JLabel();
        btnCancel = new javax.swing.JButton();
        btnOk = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        txtDuration = new javax.swing.JFormattedTextField();
        cmbType = new javax.swing.JComboBox<>();
        txtDate = new javax.swing.JFormattedTextField();
        pnlBook = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        btnBook = new javax.swing.JButton();
        btnBookFree = new javax.swing.JButton();
        lnkBook = new it.smartteaching.prototype.view.widgets.JLink();
        jPanel1 = new javax.swing.JPanel();
        editorMat = new it.smartteaching.prototype.view.widgets.JEditList();
        editorAssign = new it.smartteaching.prototype.view.widgets.JEditList();
        btnRemove = new javax.swing.JButton();

        setMinimumSize(new java.awt.Dimension(460, 420));
        setPreferredSize(new java.awt.Dimension(600, 420));

        lblTitle.setText("$HEADER");
        lblTitle.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        btnCancel.setText("Annulla");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnOk.setText("Conferma");
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel1.setText("Nome:");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel2.setText("Tipo:");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel3.setText("Durata:");

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel4.setText("Data:");

        txtName.setText("Lezione™");

        try {
            txtDuration.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtDuration.setText("00:00");
        txtDuration.setMinimumSize(new java.awt.Dimension(140, 29));

        cmbType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Presenza", "Streaming", "Solo materiale" }));
        cmbType.setMinimumSize(new java.awt.Dimension(140, 29));
        cmbType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTypeActionPerformed(evt);
            }
        });

        txtDate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm"))));
        txtDate.setText("2020-04-15 14:40");
        txtDate.setMinimumSize(new java.awt.Dimension(140, 29));

        pnlBook.setOpaque(false);

        jLabel7.setText("Prenotazione:");

        btnBook.setText("Prenota");
        btnBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBookActionPerformed(evt);
            }
        });

        btnBookFree.setText("Libera");
        btnBookFree.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBookFreeActionPerformed(evt);
            }
        });

        lnkBook.setText("impostata");
        lnkBook.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkBookMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout pnlBookLayout = new javax.swing.GroupLayout(pnlBook);
        pnlBook.setLayout(pnlBookLayout);
        pnlBookLayout.setHorizontalGroup(
            pnlBookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBookLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlBookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlBookLayout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lnkBook, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBookLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnBookFree)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBook))))
        );
        pnlBookLayout.setVerticalGroup(
            pnlBookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBookLayout.createSequentialGroup()
                .addGroup(pnlBookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(lnkBook, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBook, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBookFree, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 129, Short.MAX_VALUE))
        );

        jPanel1.setPreferredSize(new java.awt.Dimension(0, 0));
        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.Y_AXIS));
        jPanel1.add(editorMat);
        jPanel1.add(editorAssign);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addGap(6, 6, 6)
                        .addComponent(txtName))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtDuration, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pnlBook, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                                .addGap(66, 66, 66)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(cmbType, javax.swing.GroupLayout.Alignment.LEADING, 0, 144, Short.MAX_VALUE)
                                    .addComponent(txtDate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(3, 3, 3)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addGap(3, 3, 3)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cmbType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(3, 3, 3)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDuration, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addComponent(pnlBook, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        btnRemove.setText("Elimina");
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblTitle)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnRemove)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCancel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnOk)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRemove, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        ok = false;
        CONTROLLER.endLessonEdit(this);
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        if (!checkLessonConsistent())
            return;
        ok = true;
        op = nuovo? DataOperation.ADD : DataOperation.UPDATE;
        CONTROLLER.endLessonEdit(this);
    }//GEN-LAST:event_btnOkActionPerformed

    private void cmbTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTypeActionPerformed
        cust_type = getNewType();
        changeLessonType();
    }//GEN-LAST:event_cmbTypeActionPerformed

    private void btnBookFreeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBookFreeActionPerformed
        freeBooking();
    }//GEN-LAST:event_btnBookFreeActionPerformed

    private void btnBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBookActionPerformed
        askBooking();
    }//GEN-LAST:event_btnBookActionPerformed

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
        if (nuovo)
            return;
        if (JOptionPane.showConfirmDialog(this,
            "Sicuro di voler eliminare la lezione?",
            "Smart Teaching",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.WARNING_MESSAGE) != JOptionPane.YES_OPTION)
            return;
        freeBooking();
        ok = true;
        op = DataOperation.DELETE;
        CONTROLLER.endLessonEdit(this);
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void lnkBookMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lnkBookMouseClicked
        if (this.cust_link == null)
            return;
        openLink(this.cust_link.getLink());
    }//GEN-LAST:event_lnkBookMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBook;
    private javax.swing.JButton btnBookFree;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnOk;
    private javax.swing.JButton btnRemove;
    private javax.swing.JComboBox<String> cmbType;
    private it.smartteaching.prototype.view.widgets.JEditList editorAssign;
    private it.smartteaching.prototype.view.widgets.JEditList editorMat;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel lblTitle;
    private it.smartteaching.prototype.view.widgets.JLink lnkBook;
    private javax.swing.JPanel pnlBook;
    private javax.swing.JFormattedTextField txtDate;
    private javax.swing.JFormattedTextField txtDuration;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables
}
