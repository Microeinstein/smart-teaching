package it.smartteaching.prototype.view;

import it.smartteaching.prototype.Core;
import it.smartteaching.prototype.Core.Pair;
import it.smartteaching.prototype.model.classiFile.*;
import java.nio.file.Path;
import org.eclipse.swt.widgets.*;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import static it.smartteaching.prototype.control.Controller.CONTROLLER;

public class PanelMateriale extends PanelCommon {
    private final Materiale mat;
    private final DefaultListModel itemList;
    private final ListSelectionModel itemSelection;
    private final DirectoryDialog dirdialog = new DirectoryDialog(CONTROLLER.getForm().shell);

    public PanelMateriale(Materiale m) {
        super(m.nome, m.lezione.insegnamento.nome);
        initComponents();
        this.mat = m;
        itemList = new DefaultListModel();
        lstContents.setModel(itemList);
        itemSelection = lstContents.getSelectionModel();
        itemSelection.addListSelectionListener(l -> showSelectionData());
        itemSelection.setSelectionInterval(0, 0);
        dirdialog.setText("Seleziona cartella downloads");
        updateContent();
    }
    private void updateContent() {
        this.lblTitolare.setText("titolare: " + mat.lezione.insegnamento.titolare.nome);
        this.lezHeader.showData(mat.lezione);
        this.lblMatNome.setText(mat.nome);
        itemList.clear();
        for (IFile f : this.mat.contenuti)
            itemList.addElement(f);
        if (this.mat.contenuti.size() > 0)
            lstContents.setSelectedIndex(0);
    }

    public void showSelectionData() {
        int[] in = itemSelection.getSelectedIndices();
        Stream<Object> st = Arrays.stream(in)
            .filter(i -> i >= 0 && i < itemList.size())
            .mapToObj(i -> itemList.get(i));
        Core.Pair<String, String> info = IFile.getInfo(st.toArray(IFile[]::new));
        lblSize.setText(info.fst);
        lblMIME.setText(info.snd);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlMenu = new javax.swing.JPanel();
        lnkBack = new it.smartteaching.prototype.view.widgets.JLink();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lblSize = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblMIME = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        lblTitolare = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lezHeader = new it.smartteaching.prototype.view.widgets.JViewHeaderLezione();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstContents = new javax.swing.JList<>();
        btnDownload = new javax.swing.JButton();
        lblMatNome = new javax.swing.JLabel();

        pnlMenu.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 0, 1, FormCommon.BORDER_COLOR));
        pnlMenu.setMinimumSize(new java.awt.Dimension(210, 210));
        pnlMenu.setPreferredSize(new java.awt.Dimension(210, 210));
        pnlMenu.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 12, 12));

        lnkBack.setText("Indietro");
        lnkBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkBackMouseClicked(evt);
            }
        });
        pnlMenu.add(lnkBack);

        jPanel2.setPreferredSize(new java.awt.Dimension(195, 80));

        jLabel2.setText("Dimensione:");

        lblSize.setFont(new java.awt.Font("JetBrains Mono", 0, 13)); // NOI18N
        lblSize.setText("0B");

        jLabel4.setText("Tipo:");

        lblMIME.setFont(new java.awt.Font("JetBrains Mono", 0, 13)); // NOI18N
        lblMIME.setText("text/plain");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblSize, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblMIME, javax.swing.GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addGap(0, 0, 0)
                .addComponent(lblSize)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4)
                .addGap(0, 0, 0)
                .addComponent(lblMIME))
        );

        pnlMenu.add(jPanel2);

        jLabel1.setText("$HEADER");

        lblTitolare.setText("TITOLARE");

        jScrollPane2.setPreferredSize(new java.awt.Dimension(0, 0));

        lstContents.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, FormCommon.BORDER_COLOR));
        lstContents.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "item1" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        lstContents.setPreferredSize(new java.awt.Dimension(0, 0));
        jScrollPane2.setViewportView(lstContents);

        btnDownload.setText("Scarica");
        btnDownload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownloadActionPerformed(evt);
            }
        });

        lblMatNome.setText("MATERIALE");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lezHeader, javax.swing.GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnDownload))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblMatNome)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lezHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblMatNome)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDownload, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblTitolare)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlMenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lblTitolare))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlMenu, javax.swing.GroupLayout.DEFAULT_SIZE, 431, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void lnkBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lnkBackMouseClicked
        CONTROLLER.back();
    }//GEN-LAST:event_lnkBackMouseClicked

    private void btnDownloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownloadActionPerformed
        String dir = dirdialog.open();
        if (dir == null)
            return;
        Path p = Path.of(dir);
        int[] in = itemSelection.getSelectedIndices();
        String failed = Arrays.stream(in)
            .filter(i -> i >= 0 && i < itemList.size())
            .mapToObj(i -> itemList.get(i))
            .map(f -> {
                FileRemoto fr = (FileRemoto)f;
                return new Pair<FileRemoto, Boolean>(fr, fr.scaricaIn(p));
            })
            .filter(pp -> !pp.snd)
            .map(pp -> pp.fst.nome)
            .collect(Collectors.joining("\n"));
        if (!failed.isEmpty()) {
            JOptionPane.showMessageDialog(
                this,
                "Impossibile scaricare i seguenti files:\n" + failed,
                "Smart Teaching",
                JOptionPane.WARNING_MESSAGE
            );
        }
    }//GEN-LAST:event_btnDownloadActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDownload;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblMIME;
    private javax.swing.JLabel lblMatNome;
    private javax.swing.JLabel lblSize;
    private javax.swing.JLabel lblTitolare;
    private it.smartteaching.prototype.view.widgets.JViewHeaderLezione lezHeader;
    private it.smartteaching.prototype.view.widgets.JLink lnkBack;
    private javax.swing.JList<String> lstContents;
    private javax.swing.JPanel pnlMenu;
    // End of variables declaration//GEN-END:variables
}
