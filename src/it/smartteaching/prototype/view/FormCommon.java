package it.smartteaching.prototype.view;

import static it.smartteaching.prototype.Core.*;
import it.smartteaching.prototype.control.Controller;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public abstract class FormCommon extends JFrame {
    public static final Color BORDER_COLOR = new Color(183, 183, 183);
    public static final Color BG_COLOR = Color.white;
    public final Color defcolor;
    private PanelCommon current = null;

    public FormCommon() {
        this.defcolor = this.getContentPane().getBackground();
    }
    private void updateStyle() {
        if (this.current != null) {
            this.setTitle("Smart Teaching - " + this.current.title);
            current.applyStyle();
        }
    }

    public void showPanel(PanelCommon p) {
        //if (this.current != null) {
        //}
        this.setContentPane(p);
        this.current = p;
        updateStyle();
        validate();
        repaint();
    }
    public PanelCommon getPanel() {
        return current;
    }

    @Override
    public void pack() {
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        //this.setResizable(false);
        this.updateStyle();

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent evt) {
                Controller.CONTROLLER.close();
            }
        });

        super.pack();

        this.setLocationRelativeTo(null); //center the window
    }
}
