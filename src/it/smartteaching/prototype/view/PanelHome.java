package it.smartteaching.prototype.view;

import it.smartteaching.prototype.view.widgets.JViewInsegnamento;
import it.smartteaching.prototype.model.*;
import static it.smartteaching.prototype.model.Model.MODEL;
import it.smartteaching.prototype.model.classiPersone.*;
import java.util.*;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Group;
import static it.smartteaching.prototype.control.Controller.CONTROLLER;

public class PanelHome extends PanelCommon {
    private List<Insegnamento> lins;
    private boolean coinvolto;

    public PanelHome() {
        super("Home", "Home");
        initComponents();
        lblAccount.setText(CONTROLLER.getPersona().nome);
    }
    public void loadList(List<Insegnamento> lins, boolean coinvolto) {
        this.lins = lins;
        this.coinvolto = coinvolto;
        lstInsegnamenti.removeAll();
        boolean docente = CONTROLLER.isDocente();
        //boolean first = true;
        if (docente) {
            lnkWithSubscr.setText("tenuti");
            lnkWithoutSubscr.setText("non tenuti");
        }
        GroupLayout lgl = (GroupLayout)lstInsegnamenti.getLayout();
        Group gv = lgl.createSequentialGroup();
        Group gh = lgl.createParallelGroup();
        List<JViewInsegnamento> prio0 = new ArrayList<>();
        List<JViewInsegnamento> prio1 = new ArrayList<>();
        for (Insegnamento i : lins) {
            boolean responsabile = docente && i.isResponsabile((Docente)CONTROLLER.getPersona());
            JViewInsegnamento jvi = new JViewInsegnamento(i, coinvolto, responsabile);
            jvi.addShowEventHandler(this::jviNameClicked);
            jvi.addToggleSubEventHandler(this::jviToggleSubClicked);
            //System.err.println("Adding:");
            //System.err.println(i);
            //if (!first)
            //    lstInsegnamenti.add(Box.createRigidArea(new Dimension(0,6)));
            //first = false;
            if (responsabile)
                prio0.add(jvi);
            else
                prio1.add(jvi);
            setNewDefaults(jvi);
            //lstInsegnamenti.add(jvi);
        }
        prio0.addAll(prio1);
        prio0.forEach(jvi -> {
            gv.addComponent(jvi,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.DEFAULT_SIZE,
                GroupLayout.PREFERRED_SIZE
            );
            gh.addComponent(jvi);
        });
        lgl.setHorizontalGroup(gh);
        lgl.setVerticalGroup(gv);
        //lstInsegnamenti.revalidate();
        validate();
        //adaptSize();
        repaint();
    }
    /*private void adaptSize() {
        Dimension d = jScrollPane1.getSize();
        lstInsegnamenti.setSize(d);
        for (Component c : lstInsegnamenti.getComponents())
            c.setMaximumSize(new Dimension(d.width - 16, c.getHeight()));
    }*/

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitle = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lnkWithSubscr = new it.smartteaching.prototype.view.widgets.JLink();
        lnkWithoutSubscr = new it.smartteaching.prototype.view.widgets.JLink();
        lnkChat = new it.smartteaching.prototype.view.widgets.JLink();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstInsegnamenti = new javax.swing.JPanel();
        lnkBack = new it.smartteaching.prototype.view.widgets.JLink();
        lblAccount = new javax.swing.JLabel();

        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });

        lblTitle.setText("$HEADER");
        lblTitle.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jPanel2.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 0, 1, FormCommon.BORDER_COLOR));
        jPanel2.setMinimumSize(new java.awt.Dimension(0, 0));

        jLabel1.setText("Mostra insegnamenti:");

        lnkWithSubscr.setText("con iscrizione");
        lnkWithSubscr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkWithSubscrMouseClicked(evt);
            }
        });

        lnkWithoutSubscr.setText("senza iscrizione");
        lnkWithoutSubscr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkWithoutSubscrMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lnkWithSubscr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lnkWithoutSubscr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lnkWithSubscr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lnkWithoutSubscr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        lnkChat.setText("Apri chat");
        lnkChat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkChatMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lnkChat, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lnkChat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(224, Short.MAX_VALUE))
        );

        jScrollPane1.setBorder(null);
        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setViewportBorder(null);
        jScrollPane1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane1.setHorizontalScrollBar(null);
        jScrollPane1.setOpaque(false);
        jScrollPane1.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                jScrollPane1ComponentResized(evt);
            }
        });

        lstInsegnamenti.setPreferredSize(new java.awt.Dimension(0, 0));

        javax.swing.GroupLayout lstInsegnamentiLayout = new javax.swing.GroupLayout(lstInsegnamenti);
        lstInsegnamenti.setLayout(lstInsegnamentiLayout);
        lstInsegnamentiLayout.setHorizontalGroup(
            lstInsegnamentiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 351, Short.MAX_VALUE)
        );
        lstInsegnamentiLayout.setVerticalGroup(
            lstInsegnamentiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 330, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(lstInsegnamenti);

        lnkBack.setText("Logout");
        lnkBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkBackMouseClicked(evt);
            }
        });

        lblAccount.setText("ACCOUNT");
        lblAccount.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblAccount)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lnkBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 351, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTitle)
                    .addComponent(lnkBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAccount))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void lnkWithSubscrMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lnkWithSubscrMouseClicked
        CONTROLLER.showListInsegnamenti(this, true);
    }//GEN-LAST:event_lnkWithSubscrMouseClicked

    private void lnkWithoutSubscrMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lnkWithoutSubscrMouseClicked
        CONTROLLER.showListInsegnamenti(this, false);
    }//GEN-LAST:event_lnkWithoutSubscrMouseClicked

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        //adaptSize();
    }//GEN-LAST:event_formComponentResized

    private void jScrollPane1ComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_jScrollPane1ComponentResized
        //BoxLayout bl = (BoxLayout)lstInsegnamenti.getLayout();
        //bl.invalidateLayout(lstInsegnamenti);
    }//GEN-LAST:event_jScrollPane1ComponentResized

    private void lnkBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lnkBackMouseClicked
        CONTROLLER.back();
    }//GEN-LAST:event_lnkBackMouseClicked

    private void lnkChatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lnkChatMouseClicked
        CONTROLLER.openChat(null);
    }//GEN-LAST:event_lnkChatMouseClicked

    private void jviNameClicked(JViewInsegnamento jvi, java.awt.event.MouseEvent evt) {
        CONTROLLER.openInsegnamento(jvi.ins);
    }

    private void jviToggleSubClicked(JViewInsegnamento jvi, java.awt.event.MouseEvent evt) {
        if (!CONTROLLER.setSubscrInsegnamento(jvi.ins, !jvi.coinvolto))
            return;
        lstInsegnamenti.remove(jvi); //"move" to other view
        lstInsegnamenti.revalidate();
        validate();
        repaint();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblAccount;
    private javax.swing.JLabel lblTitle;
    private it.smartteaching.prototype.view.widgets.JLink lnkBack;
    private it.smartteaching.prototype.view.widgets.JLink lnkChat;
    private it.smartteaching.prototype.view.widgets.JLink lnkWithSubscr;
    private it.smartteaching.prototype.view.widgets.JLink lnkWithoutSubscr;
    private javax.swing.JPanel lstInsegnamenti;
    // End of variables declaration//GEN-END:variables
}
