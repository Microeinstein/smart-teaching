package it.smartteaching.prototype.control;

import static it.smartteaching.prototype.model.classiConferencePlatform.IConferencePlatform.CONFPLATFORM;
import static it.smartteaching.prototype.model.classiPersone.*;
import static it.smartteaching.prototype.model.classiLezione.*;
import static it.smartteaching.prototype.model.classiFile.*;
import static it.smartteaching.prototype.model.Model.MODEL;
import static it.smartteaching.prototype.Core.*;
import it.smartteaching.prototype.model.*;
import it.smartteaching.prototype.model.Model.DataOperation;
import it.smartteaching.prototype.model.classiConferencePlatform.IPrenotazione;
import it.smartteaching.prototype.view.*;
import java.time.LocalDateTime;
import java.util.*;

public class Controller {
    public static final Controller CONTROLLER = new Controller();
    private MainForm form;
    private Persona current;
    private boolean isDocente;
    private boolean already_closing = false;
    private Stack<Action> history = new Stack<>();

    public void run() {
        setLookAndFeel();
        form = new MainForm();
        showLogin();
        //autoLoad();
        form.windowClosing = (we) -> closing();
        form.setVisible(true);
    }
    public void closing() {
        if (already_closing)
            return;
        already_closing = true;
        PanelCommon pnl = form.getPanel();
        if (pnl instanceof IDataUserEditor) {
            System.err.println("undo pending");
            ((IDataUserEditor)pnl).undoPending();
        }
    }
    public void close() {
        closing();
        System.err.println("closing");
        System.exit(0);
    }
    public MainForm getForm() {
        return form;
    }
    public Persona getPersona() {
        return current;
    }
    public boolean isDocente() {
        return isDocente;
    }
    public void back() {
        if (history.size() < 2)
            return;
        history.pop();
        history.pop().call();
    }

    public void showLogin() {
        history.push(() -> showLogin());
        current = null;
        isDocente = false;
        form.showPanel(new PanelLogin());
    }
    private void autoLoad() { //DEBUG
        String id = null;
        showLogin();
        switch (1) {
            case 1: id = "professore"; break;
            case 2: id = "matricola";  break;
            case 3: id = "vr_mario";   break;
        }
        MODEL.aggiornaAccesso(id);
        current = MODEL.getIdentity(id);
        isDocente = current instanceof Docente;
        showHome();
        //openInsegnamento(MODEL.getInsegnamenti(current, true).get(0));
        //editLesson(MODEL.getInsegnamenti(current, true).get(0).getLezioni().get(0), false);
    }
    public boolean login(String matricola, String password) {
        try {
            current = MODEL.login(matricola, password);
            isDocente = current instanceof Docente;
            showHome();
            return true;
        } catch (InvalidCredentialsException ex) {
            return false;
        }
    }

    public void showHome() {
        history.push(() -> showHome());
        PanelHome ph = new PanelHome();
        showListInsegnamenti(ph, true);
        form.showPanel(ph);
    }
    public void showListInsegnamenti(PanelHome ph, boolean coinvolto) {
        List<Insegnamento> l = MODEL.getInsegnamenti(current, coinvolto);
        ph.loadList(l, coinvolto);
    }
    public boolean setSubscrInsegnamento(Insegnamento ins, boolean iscritto) {
        MODEL.modificaIscrizione((Studente)current, ins, iscritto? DataOperation.ADD : DataOperation.DELETE);
        return true;
    }

    public void openChat(Persona p) {
        if (p != null && p.equals(current))
            p = null;
        final Persona p2 = p;
        history.push(() -> openChat(p2));
        PanelChat pc = new PanelChat(p2);
        form.showPanel(pc);
    }
    public void openInsegnamento(Insegnamento ins) {
        history.push(() -> openInsegnamento(ins));
        PanelInsegnamento pi = new PanelInsegnamento(ins);
        form.showPanel(pi);
    }

    public void editLesson(Lezione lez, boolean nuovo) {
        history.push(() -> editLesson(lez, nuovo));
        PanelLezione pl = new PanelLezione(lez, nuovo);
        form.showPanel(pl);
    }
    public IPrenotazione askBooking(LocalDateTime date) {
        return CONFPLATFORM.book(date);
    }
    public void freeBooking(IPrenotazione link) {
        CONFPLATFORM.free(link);
    }
    public void endLessonEdit(PanelLezione pnl) {
        if (pnl.isApproved())
            MODEL.modificaLezione(
                pnl.applyData(),
                pnl.getChoice(),
                (Changes<Materiale>)pnl.getChanges(PanelLezione.CHANGES_MATERIAL),
                (Changes<Compito>)pnl.getChanges(PanelLezione.CHANGES_ASSIGN)
            );
        else
            pnl.undoPending();
        //openInsegnamento(pnl.ins);
        back();
    }

    public void openMateriale(Materiale m) {
        history.push(() -> openMateriale(m));
        PanelMateriale pm = new PanelMateriale(m);
        form.showPanel(pm);
    }
    public void openCompito(Compito c) {
        history.push(() -> openCompito(c));
        PanelCompito pc = new PanelCompito(c);
        form.showPanel(pc);
    }
}
